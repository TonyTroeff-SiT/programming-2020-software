﻿using Microsoft.EntityFrameworkCore;
using SiT.Social.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiT.Social
{
    class Program
    {
        static async Task Main(string[] args)
        {
            SocialContext socialContext = new SocialContext();
            bool continueWithCommandExecution = true;
            while (continueWithCommandExecution)
            {
                PrintMenu();
                var enteredCommandOption = Console.ReadLine();
                switch (enteredCommandOption)
                {
                    case "1":
                        await CreateBlogAsync(socialContext);
                        break;
                    case "2":
                        await CreatePostAsync(socialContext);
                        break;
                    case "3":
                        await ListAllBlogsAsync(socialContext);
                        break;
                    case "4":
                        await CreateTagAsync(socialContext);
                        break;
                    case "5":
                        await LinkPostToTagsAsync(socialContext);
                        break;
                    case "6":
                        await UpdateBlogAsync(socialContext);
                        break;
                    case "7":
                        await UpdatePostAsync(socialContext);
                        break;
                    case "8":
                        await DeleteBlogAsync(socialContext);
                        break;
                    case "9":
                        await DeletePostAsync(socialContext);
                        break;
                    default:
                        continueWithCommandExecution = false;
                        break;
                }
            }
        }

        private static void PrintMenu()
        {
            // CRUD
            // C - create
            // R - read
            // U - update
            // D - delete
            Console.WriteLine("1. Create a blog");
            Console.WriteLine("2. Create a post");
            Console.WriteLine("3. List all blogs with their posts");
            Console.WriteLine("4. Create a tag");
            Console.WriteLine("5. Link a post to some tags");
            Console.WriteLine("6. Update a blog");
            Console.WriteLine("7. Update a post");
            Console.WriteLine("8. Delete a blog");
            Console.WriteLine("9. Delete a post");
        }

        private static async Task CreateBlogAsync(SocialContext socialContext)
        {
            Console.WriteLine("Creating a blog.");
            Console.Write("Enter the name of the blog: ");
            string blogName = Console.ReadLine();
            Console.Write("Enter the name of the blog author: ");
            string blogAuthorName = Console.ReadLine();

            var blog = new Blog
            {
                Author = blogAuthorName,
                Name = blogName,
                CreationDate = DateTime.UtcNow
            };

            socialContext.Blogs.Add(blog);
            await socialContext.SaveChangesAsync();
            Console.WriteLine("The blog was successfully created. :)");
        }

        private static async Task CreatePostAsync(SocialContext socialContext)
        {
            Console.WriteLine("Creating a post.");
            Console.Write("Enter the unique identifier of the blog where this post belongs: ");
            int blogId = int.Parse(Console.ReadLine());
            var blog = await socialContext.Blogs.FirstOrDefaultAsync(b => b.Id == blogId);
            if (blog == null)
            {
                Console.WriteLine("No such blog is found.");
                return;
            }

            Console.Write("Enter the title of the post: ");
            string postTitle = Console.ReadLine();
            Console.Write("Enter the content of the post: ");
            string postContent = Console.ReadLine();

            var post = new Post
            {
                Title = postTitle,
                Content = postContent,
                CreationTime = DateTime.Now,
                Blog = blog
            };
            socialContext.Posts.Add(post);
            await socialContext.SaveChangesAsync();
            Console.WriteLine("The post was successfully created. :)");
        }

        private static async Task CreateTagAsync(SocialContext socialContext)
        {
            Console.WriteLine("Creating a tag.");
            Console.Write("Enter the name of the tag: ");
            string tagName = Console.ReadLine();

            var tag = new Tag
            {
                Name = tagName
            };
            socialContext.Tags.Add(tag);
            await socialContext.SaveChangesAsync();
            Console.WriteLine("The tag was successfully created. :)");
        }

        private static async Task ListAllBlogsAsync(SocialContext socialContext)
        {
            var timeThreshold = DateTime.Now.Subtract(TimeSpan.FromDays(30));
            var blogs = await socialContext.Blogs
                .Where(b => b.CreationDate > timeThreshold)
                .Include(b => b.Posts)
                    .ThenInclude(p => p.Tags)
                .ToListAsync();

            foreach (var blog in blogs)
            {
                Console.WriteLine($"ID: {blog.Id}");
                Console.WriteLine($"Name: {blog.Name}");
                Console.WriteLine($"Author: {blog.Author}");
                Console.WriteLine($"Created on: {blog.CreationDate}");

                if (blog.Posts != null && blog.Posts.Any())
                {
                    Console.WriteLine("Posts:");

                    foreach (var post in blog.Posts)
                    {
                        Console.WriteLine($"--> Title: {post.Title}");
                        Console.WriteLine($"--> Content: {post.Content}");

                        if (post.Tags != null && post.Tags.Any())
                            Console.WriteLine($"--> Tags: {string.Join(", ", post.Tags.Select(t => t.Name))}");
                        else
                            Console.WriteLine("--> This post has no tags");
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine("This blog has no posts.");
                    Console.WriteLine();
                }
            }
        }

        private static async Task LinkPostToTagsAsync(SocialContext socialContext)
        {
            Console.WriteLine("Linking a post to tags.");
            Console.Write("Enter the identifier of the post you want to link: ");
            var postId = int.Parse(Console.ReadLine());

            var post = await socialContext.Posts.FirstOrDefaultAsync(p => p.Id == postId);
            if (post == null)
            {
                Console.WriteLine("No such post exists.");
                return;
            }

            Console.Write("Enter the identifiers of the tags you want to link (separate them with a comma): ");
            var tagIdentifiers = Console.ReadLine().Split(", ", StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x));
            var tags = await socialContext.Tags.Where(t => tagIdentifiers.Contains(t.Id)).ToListAsync();
            if (tags == null || tags.Any() == false)
            {
                Console.WriteLine("No tags are found.");
                return;
            }

            post.Tags = new List<Tag>();
            foreach (var tagToLink in tags)
                post.Tags.Add(tagToLink);

            socialContext.Posts.Update(post);
            await socialContext.SaveChangesAsync();
        }

        private static async Task UpdateBlogAsync(SocialContext socialContext)
        {
            Console.WriteLine("Updating a blog.");
            Console.Write("Enter the blog identifier that you want to update: ");

            var blogId = int.Parse(Console.ReadLine());
            var blog = await socialContext.Blogs.FirstOrDefaultAsync(blog => blog.Id == blogId);
            if (blog == null)
            {
                Console.WriteLine("No such blog exists.");
                return;
            }

            Console.Write($"Enter a value indicating the new name of the blog: ({blog.Name})");
            var newName = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(newName) == false)
                blog.Name = newName;

            Console.Write($"Enter a value indicating the new author of the blog: ({blog.Author})");
            var newAuthor = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(newAuthor) == false)
                blog.Author = newAuthor;

            socialContext.Blogs.Update(blog);
            await socialContext.SaveChangesAsync();
        }

        private static async Task UpdatePostAsync(SocialContext socialContext)
        {
            Console.WriteLine("Updating a post.");
            Console.Write("Enter the post identifier that you want to update: ");

            var postId = int.Parse(Console.ReadLine());
            var post = await socialContext.Posts.FirstOrDefaultAsync(post => post.Id == postId);
            if (post == null)
            {
                Console.WriteLine("No such post exists.");
                return;
            }

            Console.Write($"Enter a value indicating the new title of the post: ({post.Title})");
            var newTitle = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(newTitle) == false)
                post.Title = newTitle;

            Console.Write($"Enter a value indicating the new content of the post: ({post.Content})");
            var newContent = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(newContent) == false)
                post.Content = newContent;

            socialContext.Posts.Update(post);
            await socialContext.SaveChangesAsync();
        }

        private static async Task DeleteBlogAsync(SocialContext socialContext)
        {
            Console.WriteLine("Deleting a blog.");
            Console.Write("Enter the blog identifier that you want to delete: ");

            var blogId = int.Parse(Console.ReadLine());
            var blog = await socialContext.Blogs
                //.Include(b => b.Posts)
                .FirstOrDefaultAsync(blog => blog.Id == blogId);
            if (blog == null)
            {
                Console.WriteLine("No such blog exists.");
                return;
            }

            //socialContext.Posts.RemoveRange(blog.Posts);
            socialContext.Blogs.Remove(blog);
            await socialContext.SaveChangesAsync();
        }

        private static async Task DeletePostAsync(SocialContext socialContext)
        {
            Console.WriteLine("Deleting a post.");
            Console.Write("Enter the post identifier that you want to delete: ");

            var postId = int.Parse(Console.ReadLine());
            var post = await socialContext.Posts.FirstOrDefaultAsync(p => p.Id == postId);
            if (post == null)
            {
                Console.WriteLine("No such post exists.");
                return;
            }

            socialContext.Posts.Remove(post);
            await socialContext.SaveChangesAsync();
        }
    }
}
