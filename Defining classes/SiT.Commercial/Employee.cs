﻿namespace SiT.Commercial
{
    public class Employee
    {
        // Name
        public string Name { get; set; }

        // Age
        public ushort Age { get; set; } // [0; 65535]

        // Experience
        public float Experience { get; set; } // 1,5; 1,25 ...

        // Job position
        public string JobPosition { get; set; }

        // Salary
        public decimal Salary { get; set; }

        // Email
        public string Email { get; set; }
    }
}
