﻿using System;
using System.Collections.Generic;

namespace SiT.Social.Models
{
    public class Blog
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public string Author { get; set; }

        public ICollection<Post>  Posts { get; set; }
    }
}
