﻿using Microsoft.EntityFrameworkCore;
using SiT.WebDevelopmentDemo.Data.Interfaces;
using SiT.WebDevelopmentDemo.Data.Models;

namespace SiT.WebDevelopmentDemo.Data.Repositories
{
    public class PerformanceRepository : BaseRepository<Performance>, IPerformanceRepository
    {
        public PerformanceRepository(DemoDbContext dbContext) : base(dbContext)
        {
        }

        protected override DbSet<Performance> DbSet => this.DbContext.Performances;
    }
}
