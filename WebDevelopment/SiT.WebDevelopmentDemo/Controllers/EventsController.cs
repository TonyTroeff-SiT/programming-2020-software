﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using SiT.WebDevelopmentDemo.Core.Interfaces;
using SiT.WebDevelopmentDemo.Data.Models;
using SiT.WebDevelopmentDemo.ViewModels;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SiT.WebDevelopmentDemo.Controllers
{
    [Route("events")]
    public class EventsController : Controller
    {
        private readonly IEventService _eventService;

        public EventsController(IEventService eventService)
        {
            this._eventService = eventService ?? throw new ArgumentNullException(nameof(eventService));
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllEvents(CancellationToken cancellationToken)
        {
            var events = await this._eventService.GetAllEventsAsync(cancellationToken);
            return this.View(events);
        }

        [HttpGet("[action]")]
        public IActionResult Create() => this.View();

        //[HttpPost("[action]")]
        //public async Task<IActionResult> Create(CreateEventInputModel inputModel, CancellationToken cancellationToken, [FromServices] IValidator<CreateEventInputModel> validator)
        //{
        //    var validationResult = validator.Validate(inputModel);
        //    if (validationResult.IsValid == false)
        //        return this.BadRequest(validationResult.ToString());

        //    var eventInstance = new Event { Name = inputModel.Name, Host = inputModel.Host };
        //    await this._eventService.CreateEventAsync(eventInstance, cancellationToken);

        //    return this.RedirectToAction(nameof(this.GetAllEvents));
        //}

        [HttpPost("[action]")]
        public async Task<IActionResult> Create(CreateEventInputModel inputModel, CancellationToken cancellationToken)
        {
            if (this.ModelState.IsValid == false)
                return this.View(inputModel);

            var eventInstance = new Event { Name = inputModel.Name, Host = inputModel.Host };
            await this._eventService.CreateEventAsync(eventInstance, cancellationToken);

            return this.RedirectToAction(nameof(this.GetAllEvents));
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Update([FromQuery] int eventId, CancellationToken cancellationToken)
        {
            var eventToUpdate = await this._eventService.GetByIdAsync(eventId, cancellationToken);

            // TODO: Think what should happen when the event is not found.
            var updateModel = new UpdateEventInputModel
            {
                Id = eventToUpdate.Id,
                Name = eventToUpdate.Name,
                Host = eventToUpdate.Host
            };
            return this.View(updateModel);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Update([FromForm] UpdateEventInputModel updateEventInputModel, CancellationToken cancellationToken)
        {
            if (this.ModelState.IsValid == false)
                return this.View(updateEventInputModel);

            var updatedEvent = new Event()
            {
                Id = updateEventInputModel.Id,
                Name = updateEventInputModel.Name,
                Host = updateEventInputModel.Host
            };
            await this._eventService.UpdateEventAsync(updatedEvent, cancellationToken);
            return this.RedirectToAction(nameof(this.GetAllEvents));
        }

        [HttpGet("[action]")]   // GET /events/GetEventsForHost?hostName={hostName}
        public async Task<IActionResult> GetEventsForHost([FromQuery] string hostName, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(hostName))
                return this.View(Enumerable.Empty<Event>());

            var events = await this._eventService.GetEventsForHostAsync(hostName, cancellationToken);
            return this.View(events);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Delete([FromQuery] int id, CancellationToken cancellationToken)
        {
            await this._eventService.DeleteEventAsync(id, cancellationToken);

            return this.RedirectToAction(nameof(this.GetAllEvents));
        }
    }
}
