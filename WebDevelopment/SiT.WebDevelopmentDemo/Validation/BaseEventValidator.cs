﻿using FluentValidation;
using SiT.WebDevelopmentDemo.ViewModels;

namespace SiT.WebDevelopmentDemo.Validation
{
    //public abstract class BaseEventValidator<TInputModel> : AbstractValidator<TInputModel>
    //    where TInputModel : BaseEventInputModel
    //{
    //    public BaseEventValidator()
    //    {
    //        this.RuleFor(e => e.Name).NotNull().NotEmpty().MinimumLength(5).MaximumLength(50);
    //        this.RuleFor(e => e.Host).NotNull().NotEmpty().MaximumLength(20);
    //    }
    //}

    public class BaseEventValidator : AbstractValidator<BaseEventInputModel>
    {
        public BaseEventValidator()
        {
            this.RuleFor(e => e.Name).NotNull().NotEmpty().MinimumLength(5).MaximumLength(50);
            this.RuleFor(e => e.Host).NotNull().NotEmpty().MaximumLength(20);
        }
    }
}
