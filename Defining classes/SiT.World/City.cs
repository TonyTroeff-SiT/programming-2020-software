﻿using System;
using System.Collections.Generic;

namespace SiT.World
{
    public class City : BaseNameIdentifiable
    {
        private List<Home> homes = new List<Home>();

        // Population
        public ulong Population { get; set; }

        // NameOfMajor
        public string NameOrMajor { get; set; }

        // IsDistrict
        public bool IsDiscrict { get; set; }

        public List<Home> Homes
        {
            get
            {
                return this.homes;
            }
            set
            {
                if (value is null)
                    throw new ArgumentNullException("Cannot set null to the `Homes` property of any city.");

                this.homes = value;
            }
        }
    }
}
