﻿using SiT.Gaming.Interfaces;

namespace SiT.Gaming.Implementations
{
    public class Chess : BaseGame, IBoardGame
    {
        public override string Name => "Chess";
    }
}
