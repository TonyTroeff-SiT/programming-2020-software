﻿using SiT.Gaming.Interfaces;

namespace SiT.Gaming.Implementations
{
    public class Belot : BaseCardGame, ICardGame
    {
        public override string Name => "Belot";

        public override ushort RequiredNumberOfCards => 32;
    }
}
