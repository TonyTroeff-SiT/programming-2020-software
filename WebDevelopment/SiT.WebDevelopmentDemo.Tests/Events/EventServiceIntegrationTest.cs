﻿using SiT.WebDevelopmentDemo.Data.Models;
using SiT.WebDevelopmentDemo.Tests.Infrastructure;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace SiT.WebDevelopmentDemo.Tests.Events
{
    public abstract class EventServiceIntegrationTest : ServiceBaseTest
    {
        protected EventServiceIntegrationTest(ITestDatabaseProvider testDatabaseProvider, ITestOutputHelper testOutputHelper) 
            : base(testDatabaseProvider, testOutputHelper)
        {
        }

        [Fact]
        public async Task CreateShouldWorkCorrectly()
        {
            var eventToCreate = new Event
            {
                Name = "My test event",
                Host = "SiT"
            };
            await this.EventService.CreateEventAsync(eventToCreate);

            var retrievedEvent = await this.EventService.GetByIdAsync(eventToCreate.Id);
            Assert.NotNull(retrievedEvent);

            // We can use CompareNETObjects to extract the comparison/assertion logic.
            Assert.Equal("My test event", retrievedEvent.Name);
            Assert.Equal("SiT", retrievedEvent.Host);
        }

        [Fact]
        // [Theory]
        public void Test()
        {
            Assert.NotNull(this.EventService);

            this.TestOutputHelper.WriteLine("My message");

            // Assert.NotNull();
            // Assert.Null();
            // Assert.NotEmpty();
            // Assert.Empty();
            // Assert.Equal();
            // Assert.NotEqual();
        }

        [Fact]
        public void X()
        {
        }
    }
}
