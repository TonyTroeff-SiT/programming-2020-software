﻿namespace SiT.WebDevelopmentDemo.ViewModels
{
    public class BaseEventInputModel
    {
        public string Name { get; set; }
        public string Host { get; set; }
    }
}
