﻿using System.Collections.Generic;

namespace SiT.Gaming.Interfaces
{
    public interface IGame
    {
        string Name { get; }

        string GetRules();

        IEnumerable<IPlayer> Players { get; }

        int PlayersCount { get; }
    }
}
