﻿namespace SiT.Abstract
{
    // Classes can inherit only one class.
    // Classes can inherit many interfaces.
    public class InheritorA : BaseClass, IInterface, IPlayable
    {
        public override int Identifier { get; set; }
        public int MyFavoriteNumber { get; set; }

        // This is a valid syntax:
        //private int identifier;
        //public override int Identifier 
        //{ 
        //    get => identifier; 
        //    set => identifier = value; 
        //}

        // This is an invalid syntax (setter is missing):
        //public override int Identifier
        //{
        //    get => identifier;
        //}

        public override string GetSomeText()
        {
            return "This is Ineritor A";
        }

        public override decimal GetSomeMoney()
        {
            return 5.64m;
        }

        public string MyFavoriteNovel(string author)
        {
            return $"My fav book from {author}";
        }

        public void PlayWithMe()
        {
            System.Console.WriteLine("Play with instance A");
        }
    }
}
