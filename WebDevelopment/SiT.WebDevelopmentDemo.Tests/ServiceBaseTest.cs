﻿using Microsoft.Extensions.DependencyInjection;
using SiT.WebDevelopmentDemo.Core.Interfaces;
using SiT.WebDevelopmentDemo.Tests.Infrastructure;
using Xunit.Abstractions;

namespace SiT.WebDevelopmentDemo.Tests
{
    public class ServiceBaseTest : BaseTest
    {
        protected IEventService EventService => this.ServiceProvider.GetRequiredService<IEventService>();

        public ServiceBaseTest(ITestDatabaseProvider testDatabaseProvider, ITestOutputHelper testOutputHelper) 
            : base(testDatabaseProvider, testOutputHelper)
        {
        }
    }
}
