console.log("Successfully linked to my website");

// DOM - Document Object Model
// <node>.getElementById() - Get any DOM Node by id.
// <node>.getElementsByTagName() - Get any DOM Nodes by its tag name.
// <node>.getElementsByClassName() - Get any DOM Nodes by class name.
// <node>.querySelector() - Get any DOM Node that corresponds to the requested selector.
// <node>.querySelectorAll() - Get all DOM Nodes that corresponds to the requested selector.

let numberOfListItems = 0;

const book = document.querySelector("div#book");
const bookTitle = book.querySelector("h1.book-title");
console.log(book);
console.log(bookTitle);

// Change the inner html
// bookTitle.innerHTML = 'Not anymore';

// Add attribute
// bookTitle.setAttribute('my-custom-attribute', 'my-custom-value')

// Remove attribute
// bookTitle.removeAttribute('class');

const button = book.querySelector("button");
button.addEventListener("click", prepareNewListItem);
console.log(button.classList);
console.log(button.className);
console.log(button.id);

let buttonMargin = 1;
function moveButton() {
    if (buttonMargin >= 200)
        return;
    button.style.marginLeft = `${buttonMargin++}px`;

    setTimeout(() => {
        moveButton();
    }, 50)
}
moveButton();

function prepareNewListItem() {
  const orderedList = book.querySelector("ol");
  const listItem = document.createElement("li");
  const text = `This is automatic list item No ${numberOfListItems++}`;

  // listItem.innerHTML = text;
  const textNode = document.createTextNode(text);
  listItem.appendChild(textNode);

  orderedList.appendChild(listItem);
}
