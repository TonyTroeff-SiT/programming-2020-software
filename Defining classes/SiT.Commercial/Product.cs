﻿namespace SiT.Commercial
{
    // Hierarchy:
    // Product
    // - Phone - Call(), TakePhoto()
    // - TV - StartSmartTv, Start3d()
    // - Computer - ShutDown(), Start(), Restart()
    // - Clothes

    // Abstract classes cannot be instantiated. They can only be implemented by other classes.
    public abstract class Product
    {
        // Name
        public string Name { get; set; }

        // Price
        public decimal Price { get; set; }

        // Quality - [0; 10]
        public float Quality { get; set; }

        // Name of Origin Country
        public string MadeIn { get; set; }

        // Unique number - [1...Infinity]
        public ulong UniqueNumber { get; set; }
    }
}
