﻿using Microsoft.EntityFrameworkCore;
using SiT.WebDevelopmentDemo.Data.Interfaces;
using SiT.WebDevelopmentDemo.Data.Models;

namespace SiT.WebDevelopmentDemo.Data.Repositories
{
    public class NoteRepository : BaseRepository<Note>, INoteRepository
    {
        public NoteRepository(DemoDbContext dbContext) : base(dbContext)
        {
        }

        protected override DbSet<Note> DbSet => this.DbContext.Notes;
    }
}
