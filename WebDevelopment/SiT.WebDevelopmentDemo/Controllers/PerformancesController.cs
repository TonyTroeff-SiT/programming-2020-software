﻿using Microsoft.AspNetCore.Mvc;
using SiT.WebDevelopmentDemo.Core.Interfaces;
using SiT.WebDevelopmentDemo.Data.Models;
using SiT.WebDevelopmentDemo.ViewModels.Performances;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SiT.WebDevelopmentDemo.Controllers
{
    [Route("performance")]
    public class PerformancesController : Controller
    {
        private readonly IPerformanceService _performanceService;
        private readonly IEventService _eventService;

        public PerformancesController(IPerformanceService performanceService, IEventService eventService)
        {
            this._performanceService = performanceService ?? throw new ArgumentNullException(nameof(performanceService));
            this._eventService = eventService ?? throw new ArgumentNullException(nameof(eventService));
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetForEvent([FromQuery] int eventId, CancellationToken cancellationToken)
        {
            var performances = await this._performanceService.GetAllForEventAsync(eventId, cancellationToken);

            var viewModels = new List<PerformanceViewModel>();
            foreach (var performance in performances)
            {
                var viewModel = new PerformanceViewModel
                {
                    Id = performance.Id,
                    Name = performance.Name,
                    Perfromer = performance.PerformerName,
                    Duration = performance.Duration
                };
                viewModels.Add(viewModel);
            }

            var presetnation = new PerformancesPresentation
            {
                EventId = eventId,
                Perfromances = viewModels
            };
            return this.View(presetnation);
        }

        [HttpGet("[action]")]
        public IActionResult Create([FromQuery] int eventId)
        {
            return this.View(new PerformanceInputModel() { EventId = eventId });
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Create(PerformanceInputModel inputModel, CancellationToken cancellationToken = default)
        {
            if (inputModel == null)
                return this.BadRequest();

            var principalEvent = await this._eventService.GetByIdAsync(inputModel.EventId, cancellationToken);
            if (principalEvent == null)
                return this.BadRequest("Event with that identifier does not exist.");

            var performance = new Performance
            {
                Name = inputModel.Name,
                PerformerName = inputModel.Performer,
                Duration = inputModel.Duration,
                Event = principalEvent
            };
            await this._performanceService.CreateAsync(performance, cancellationToken);
            return this.RedirectToAction(nameof(GetForEvent), new { inputModel.EventId });
        }
    }
}
