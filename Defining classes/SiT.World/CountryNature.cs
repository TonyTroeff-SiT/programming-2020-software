﻿namespace SiT.World
{
    public enum CountryNature : byte
    {
        Warm,
        Cold,
        Windy,
        Rainy,
        Dry,
        Wet,
        Sunny,
        Cloudy
    }
}
