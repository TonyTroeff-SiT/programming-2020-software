﻿using System.Collections.Generic;

namespace SiT.Library
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ushort Age { get; set; }
        public string Nationality { get; set; }

        public ICollection<Book> Books { get; set; }
    }
}
