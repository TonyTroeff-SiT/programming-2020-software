﻿namespace SiT.World
{
    // Inheritance:
    // 1. Signature of the class `public class Room`
    // 2. ":"
    // 3. Name of the class being inherited

    // All public, internal, protected members of the base class (properties, fields, methods and constructors)
    // are accessible from the class that inherits them.
    public class Room : BaseEntity
    {
        // Inherited constructors:
        // 1. Signature of the constructor `public Room()`
        // 2. ":"
        // 3. The keyword "base"
        // 4. Opening and closing braces with arguments between them.
        public Room()
            : base(45.9)
        {
        }

        // Call the constructors separately one by one, starting with the deepest.
        // 1. BaseEntity(double area) -> We have to explicitly to pass this value as it should be taken from somewhere.
        // 2. Room()
        public Room(double area) : base(area)
        {
        }

        //public double Area { get; set; }
        public RoomType Type { get; set; }
        public int NuberOfBeds { get; set; }

        public decimal CalculatePricePerNight()
        {
            // Area * 3.14;
            return (decimal)this.Area * 3.14m;
        }
    }
}
