﻿namespace SiT.ExpenseTracker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using SiT.ExpenseTracker.Models;
    using SiT.ExpenseTracker.ViewModels;

    public class Program
    {
        static async Task Main()
        {
            var expenseTrackerContext = new ExpenseTrackerContext();
            var continueWithCommandExecution = true;
            while (continueWithCommandExecution)
            {
                PrintMenu();
                var enteredCommandOption = Console.ReadLine();
                switch (enteredCommandOption)
                {
                    case "1":
                        await CreateCustomerAsync(expenseTrackerContext);
                        break;
                    case "2":
                        await CreateAccountAsync(expenseTrackerContext);
                        break;
                    case "3":
                        await CreateTransferAsync(expenseTrackerContext);
                        break;
                    case "4":
                        await GetSummaryForCustomerAsync(expenseTrackerContext);
                        break;
                    case "5":
                        await CreateTagAsync(expenseTrackerContext);
                        break;
                    case "6":
                        await GetSummaryForAllTransfersAsync(expenseTrackerContext);
                        break;
                    case "7":
                        await GetSummaryForAllTransfersGroupedByTagsAsync(expenseTrackerContext);
                        break;
                    case "8":
                        await GetAllCustomersSummaryAsync(expenseTrackerContext);
                        break;
                    default:
                        continueWithCommandExecution = false;
                        break;
                }
            }
        }

        private static void PrintMenu()
        {
            // CRUD
            // C - create
            // R - read
            // U - update
            // D - delete
            Console.WriteLine("1. Create a customer");
            Console.WriteLine("2. Create an account");
            Console.WriteLine("3. Create a transfer");
            Console.WriteLine("4. Get summary for customer");
            Console.WriteLine("5. Create a tag");
            Console.WriteLine("6. Get summary for all transfers");
            Console.WriteLine("7. Get summary for all transfers grouped by tags");
            Console.WriteLine("8. Get all customers summary");
        }

        private static async Task CreateCustomerAsync(ExpenseTrackerContext context)
        {
            Console.WriteLine("Creating a customer.");

            Console.Write("Enter your username: ");
            var username = Console.ReadLine();

            Console.Write("Enter your password: ");
            var password = Console.ReadLine();

            var customer = new Customer
            {
                Username = username,
                Password = password,
                CreatedAt = DateTime.UtcNow
            };

            context.Customers.Add(customer);
            await context.SaveChangesAsync();
        }

        private static async Task CreateAccountAsync(ExpenseTrackerContext context)
        {
            Console.WriteLine("Creating a account.");

            Console.Write("Enter the customer identifier for the user to whom this account relates: ");
            var customerId = int.Parse(Console.ReadLine());

            var customer = await context.Customers.FirstOrDefaultAsync(c => c.Id == customerId);
            if (customer is null)
            {
                Console.WriteLine("This customer does not exist.");
                return;
            }

            Console.Write("Enter the name of the account: ");
            var name = Console.ReadLine();

            Console.Write("Enter the IBAN for the account: ");
            var iban = Console.ReadLine();

            var nameIsTaken = await context.Accounts.AnyAsync(a => a.Name == name && a.IBAN == iban);
            if (nameIsTaken)
            {
                Console.WriteLine("Account with that name and IBAN alreay exists.");
                return;
            }

            var account = new Account
            {
                Name = name,
                IBAN = iban,
                Customer = customer
            };

            context.Accounts.Add(account);
            await context.SaveChangesAsync();
        }

        private static async Task CreateTransferAsync(ExpenseTrackerContext context)
        {
            Console.WriteLine("Creating a transfer.");

            Console.Write("Enter the unique identifier of the account for which this transfer is meant: ");
            var accountId = int.Parse(Console.ReadLine());

            var account = await context.Accounts.FirstOrDefaultAsync(a => a.Id == accountId);
            if (account is null)
            {
                Console.WriteLine("Such account does not exist.");
                return;
            }

            Console.Write("Enter the amount of the transfer: ");
            var amount = decimal.Parse(Console.ReadLine());

            Console.Write("Enter a reason for the transfer: ");
            var reason = Console.ReadLine();

            Console.Write("Enter the tag identifiers for this transfer (separated by a comma and a space): ");
            var tagIdentifiers = Console.ReadLine().Split(", ", StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList();

            List<Tag> transferTags = null;
            if (tagIdentifiers.Count > 0)
                transferTags = await context.Tags.Where(t => tagIdentifiers.Contains(t.Id)).ToListAsync();

            var transfer = new Transfer
            {
                Amount = amount,
                Reason = reason,
                Account = account,
                Tags = transferTags
            };

            context.Transfers.Add(transfer);
            await context.SaveChangesAsync();
        }

        private static async Task GetSummaryForCustomerAsync(ExpenseTrackerContext context)
        {
            Console.Write("Enter the customer identifier: ");
            var customerId = int.Parse(Console.ReadLine());

            //var customer = await context.Customers.Include(c => c.Accounts).ThenInclude(a => a.Transfers).FirstOrDefaultAsync(c => c.Id == customerId);
            //if (customer is null)
            //{
            //    Console.WriteLine("No such customer exists.");
            //    return;
            //}

            //if (customer.Accounts is null)
            //{
            //    Console.WriteLine("This customer does not relate to an account.");
            //    return;
            //}

            //var transfers = customer.Accounts.SelectMany(a => a.Transfers).ToList() ?? new List<Transfer>();
            //Console.WriteLine($"Total transfers: {transfers.Count}");
            //Console.WriteLine($"Total amount: {transfers.Sum(t => t.Amount):F2}");
            //Console.WriteLine($"Min transfer: {transfers.Min(t => t.Amount)}");
            //Console.WriteLine($"Max transfer: {transfers.Max(t => t.Amount)}");

            var customer = await context.Customers
                .Select(c =>
                     new
                     {
                         Id = c.Id,
                         TotalTransfers = c.Accounts.SelectMany(a => a.Transfers).Count(),
                         TotalAmount = c.Accounts.SelectMany(a => a.Transfers).Sum(t => t.Amount),
                         MinTransfer = c.Accounts.SelectMany(a => a.Transfers).Min(t => t.Amount),
                         MaxTransfer = c.Accounts.SelectMany(a => a.Transfers).Max(t => t.Amount)
                     }
                )
                .FirstOrDefaultAsync(c => c.Id == customerId);
            if (customer is null)
            {
                Console.WriteLine("No such customer exists.");
                return;
            }

            Console.WriteLine($"Total transfers: {customer.TotalTransfers}");
            Console.WriteLine($"Total amount: {customer.TotalAmount}");
            Console.WriteLine($"Min transfer: {customer.MinTransfer}");
            Console.WriteLine($"Max transfer: {customer.MaxTransfer}");
        }

        private static async Task CreateTagAsync(ExpenseTrackerContext context)
        {
            Console.WriteLine("Creating a tag.");

            Console.Write("Enter the name of the tag: ");
            var name = Console.ReadLine();

            // Ensure that the collation supports Case-Insensitive comparison.
            var nameExists = await context.Tags.AnyAsync(t => t.Name == name);
            if (nameExists)
            {
                Console.WriteLine("This name is already taken.");
                return;
            }

            var tag = new Tag
            {
                Name = name
            };

            context.Tags.Add(tag);
            await context.SaveChangesAsync();
        }

        private static async Task GetSummaryForAllTransfersAsync(ExpenseTrackerContext context)
        {
            Console.Write("Enter the unique identifier of the customer: ");
            var customerId = int.Parse(Console.ReadLine());

            var customer = await context.Customers
                .Select(c => new
                {
                    c.Id,
                    Transfers = c.Accounts
                        .SelectMany(a => a.Transfers)
                        .Select(t => new
                        {
                            t.Reason,
                            t.Amount,
                            Tags = t.Tags.Select(tg => tg.Name)
                        })
                })
                .FirstOrDefaultAsync(c => c.Id == customerId);
            if (customer is null)
            {
                Console.WriteLine("No such customer exists.");
                return;
            }

            foreach (var transfer in customer.Transfers)
            {
                Console.WriteLine(transfer.Reason);
                Console.WriteLine(transfer.Amount);

                if (transfer.Tags.Any())
                    Console.WriteLine(string.Join(", ", transfer.Tags));
                else
                    Console.WriteLine("No tags for this transfer.");

                Console.WriteLine();
            }
        }

        private static async Task GetSummaryForAllTransfersGroupedByTagsAsync(ExpenseTrackerContext context)
        {
            Console.Write("Enter the unique identifier of the customer: ");
            var customerId = int.Parse(Console.ReadLine());

            var customer = await context.Customers
                .Select(c => new
                {
                    c.Id,
                    Transfers = c.Accounts
                        .SelectMany(a => a.Transfers)
                        .Select(t => new
                        {
                            ViewModel = new TransferViewModel
                            {
                                Id = t.Id,
                                Amount = t.Amount,
                                Reason = t.Reason
                            },
                            Tags = t.Tags.Select(x => new
                            {
                                ViewModel = new TagViewModel
                                {
                                    Id = x.Id,
                                    Name = x.Name
                                }
                            })
                        })
                })
                .FirstOrDefaultAsync(c => c.Id == customerId);
            if (customer is null)
            {
                Console.WriteLine("No such customer exists.");
                return;
            }

            // | Key | Value |
            // |  1  | First |
            // With dictionaries: O(1)
            // With lists/arrays, etc.: O(n)

            var tags = new Dictionary<int, TagViewModel>();
            var uncategorizedTransfers = new TagViewModel { Id = -1, Name = "Uncategorized" };
            tags[uncategorizedTransfers.Id] = uncategorizedTransfers;

            foreach (var transfer in customer.Transfers)
            {
                if (transfer.Tags is null || transfer.Tags.Any() == false)
                {
                    uncategorizedTransfers.AddTransferViewModel(transfer.ViewModel);
                    continue;
                }

                foreach (var tag in transfer.Tags)
                {
                    if (tag?.ViewModel is null)
                        return;

                    if (tags.TryGetValue(tag.ViewModel.Id, out var registeredViewModel) == false)
                    {
                        registeredViewModel = tag.ViewModel;
                        tags[tag.ViewModel.Id] = registeredViewModel;
                    }

                    registeredViewModel.AddTransferViewModel(transfer.ViewModel);
                }
            }

            foreach (var tag in tags.Values)
            {
                Console.WriteLine($"Tag: {tag.Name}");

                foreach (var transfer in tag.Transfers)
                {
                    Console.WriteLine("  - - - - - - - - - -");
                    Console.WriteLine($"  Reason: {transfer.Reason}");
                    Console.WriteLine($"  Amount: {transfer.Amount:F2}");
                }

                Console.WriteLine();
            }
        }

        private static async Task GetAllCustomersSummaryAsync(ExpenseTrackerContext context)
        {
            var customersInfo = await context.Customers.Select(c => new
            {
                c.Id,
                c.Username,
                Ballance = c.Accounts.SelectMany(a => a.Transfers).Sum(t => t.Amount),
                IncomesCount = c.Accounts.SelectMany(a => a.Transfers).Count(t => t.Amount > 0),
                OutcomesCount = c.Accounts.SelectMany(a => a.Transfers).Count(t => t.Amount < 0)
            }).ToListAsync();

            foreach (var customer in customersInfo)
            {
                Console.WriteLine(customer.Username);
                Console.WriteLine(customer.Ballance);
                Console.WriteLine(customer.IncomesCount);
                Console.WriteLine(customer.OutcomesCount);
            }
        }
    }
}