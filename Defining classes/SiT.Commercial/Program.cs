﻿namespace SiT.Commercial
{
    class Program
    {
        static void Main(string[] args)
        {
            var company = new Company();
            company.Name = "Microsoft";

            // Cannot instantiate abstract classes - use some implementing type.
            //var product = new Product();
            //product.Name = "Windows OS";

            var computer = new Computer();
            computer.Name = "Dell G7 17";
            computer.Processor = "Intel Core i9 10th Gen";

            var employee = new Employee();
            employee.Name = "Bill Gates";
        }
    }
}
