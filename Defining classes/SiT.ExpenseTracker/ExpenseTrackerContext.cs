﻿namespace SiT.ExpenseTracker
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using SiT.ExpenseTracker.Models;

    public class ExpenseTrackerContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transfer> Transfers { get; set; }
        public DbSet<Tag> Tags { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder
                .UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=SiT.ExpenseTracker;Trusted_Connection=True;")
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>(customer =>
            {
                customer.HasMany(c => c.Accounts)
                    .WithOne(a => a.Customer)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            // This is the same as the configuration above.
            //modelBuilder.Entity<Account>(account =>
            //{
            //    account.HasOne(a => a.Customer)
            //        .WithMany(c => c.Accounts)
            //        .OnDelete(DeleteBehavior.Cascade);
            //});

            modelBuilder.Entity<Account>(account =>
            {
                account.HasMany(a => a.Transfers)
                    .WithOne(t => t.Account)
                    .OnDelete(DeleteBehavior.Cascade);

                account.HasIndex(a => new { a.Name, a.IBAN })
                    .IsUnique(unique: true);
            });

            modelBuilder.Entity<Transfer>(transfer =>
            {
                transfer.HasIndex(t => t.Amount);
            });

            modelBuilder.Entity<Tag>(tag =>
            {
                tag.HasIndex(t => t.Name)
                    .IsUnique(unique: true);
            });
        }
    }
}