﻿namespace SiT.ExpenseTracker.Models
{
    using System;
    using System.Collections.Generic;

    public class Account
    {
        private List<Transfer> _transfers = new List<Transfer>();

        public int Id { get; set; }

        public string Name { get; set; }
        public string IBAN { get; set; }
        public DateTime ValidUntil { get; set; }

        public Customer Customer { get; set; }
        public IEnumerable<Transfer> Transfers
        {
            get => this._transfers.AsReadOnly();

            // Potentially this should not exist.
            set
            {
                if (value == null)
                    return;

                this._transfers = new List<Transfer>(value);
            }
        }

        public void AddTransfer(Transfer transfer)
        {
            if (transfer == null)
                return;

            this._transfers.Add(transfer);
        }
    }
}