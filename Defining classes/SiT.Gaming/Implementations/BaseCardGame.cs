﻿using SiT.Gaming.Interfaces;

namespace SiT.Gaming.Implementations
{
    public abstract class BaseCardGame : BaseGame, ICardGame
    {
        public abstract ushort RequiredNumberOfCards { get; }
    }
}
