﻿namespace SiT.Abstract
{
    public class InheritorB : BaseClass
    {
        public override int Identifier { get; set; }

        // When we implement an abstract method we have to be careful for:
        // 1. Preserve the signature.
        // 2. "override"
        // 3. The implementation itself.
        public override string GetSomeText()
        {
            return "This is Ineritor B";
        }

        public override decimal GetSomeMoney()
        {
            var baseResult = base.GetSomeMoney();
            return baseResult * 2;
        }

        public int GetSomeNumber()
        {
            return 5;
        }
    }
}
