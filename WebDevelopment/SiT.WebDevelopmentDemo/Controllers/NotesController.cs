﻿using Microsoft.AspNetCore.Mvc;
using SiT.WebDevelopmentDemo.Core.Interfaces;
using SiT.WebDevelopmentDemo.Data.Models;
using SiT.WebDevelopmentDemo.ViewModels;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SiT.WebDevelopmentDemo.Controllers
{
    [Route("notes")]
    public class NotesController : Controller
    {
        private readonly INoteService _noteService;

        public NotesController(INoteService noteService)
        {
            this._noteService = noteService ?? throw new ArgumentNullException(nameof(noteService));
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllNotes(CancellationToken cancellationToken)
        {
            var notes = await this._noteService.GetAllNotesAsync(cancellationToken);
            return this.View(notes);
        }

        [HttpGet("[action]")]
        public IActionResult Create() => this.View();

        [HttpPost("[action]")]
        public async Task<IActionResult> Create(CreateNoteInputModel inputModel, CancellationToken cancellationToken)
        {
            var note = new Note
            {
                Text = inputModel.Text,
                Theme = inputModel.Theme
            };
            await this._noteService.CreateAsync(note, cancellationToken);
            return this.RedirectToAction(nameof(this.GetAllNotes));
        }
    }
}
