﻿using System.Collections.Generic;

namespace SiT.ExpenseTracker.Models
{
    // [Index("Amount")]
    public class Transfer
    {
        public int Id { get; set; }

        public decimal Amount { get; set; }
        public string Reason { get; set; }

        public Account Account { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
    }
}