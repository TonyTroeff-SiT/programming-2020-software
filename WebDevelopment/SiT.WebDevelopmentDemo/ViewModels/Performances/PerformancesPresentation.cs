﻿using System.Collections.Generic;

namespace SiT.WebDevelopmentDemo.ViewModels.Performances
{
    public class PerformancesPresentation
    {
        public int EventId { get; set; }
        public IEnumerable<PerformanceViewModel> Perfromances { get; set; }
    }
}
