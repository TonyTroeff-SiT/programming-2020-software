﻿namespace SiT.WebDevelopmentDemo.ViewComponents
{
    using Microsoft.AspNetCore.Mvc;
    using SiT.WebDevelopmentDemo.Data.Models;

    public class EventViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(Event eventToRender)
        {
            return this.View(eventToRender);
        }
    }
}
