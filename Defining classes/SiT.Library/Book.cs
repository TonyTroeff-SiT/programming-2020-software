﻿namespace SiT.Library
{
    public class Book
    {
        // a -> b
        // a -> c

        // Every entity should have a unique identifier.
        public int Id { get; set; }

        public string Name { get; set; }
        public double Rating { get; set; }

        // How to ensure relation?
        // 1. Add a property that relates to the unique identifier of the principal entity.
        public int AuthorId { get; set; }

        // 2. Add a property that relates to the principal entity itself.
        public Author Author { get; set; }

        // 3. Add a property to the principal entity model referencing the collection of dependent entities.
    }
}
