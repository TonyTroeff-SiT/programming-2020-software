﻿namespace SiT.WebDevelopmentDemo.Data.Interfaces
{
    using SiT.WebDevelopmentDemo.Data.Models;

    public interface IEventRepository : IRepository<Event>
    {
    }
}
