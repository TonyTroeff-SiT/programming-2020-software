﻿using System;
using System.Collections.Generic;

namespace SiT.World
{
    public class Home : BaseEntity
    {
        private List<Room> rooms = new List<Room>();

        public string Address { get; set; }
        public List<Room> Rooms
        {
            get
            {
                if (this.rooms is null)
                    this.rooms = new List<Room>();

                return this.rooms;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Cannot set null values for the rooms of any home");

                this.rooms = value;
            }
        }
        public int RoomsCount
        {
            get
            {
                return this.rooms.Count;
            }
        }
    }
}
