﻿namespace SiT.ExpenseTracker.Models
{
    using System;
    using System.Collections.Generic;

    public class Customer
    {
        public int Id { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public string PreferredLanguage { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastSignedInAt { get; set; }
        
        public IEnumerable<Account> Accounts { get; set; }
    }
}