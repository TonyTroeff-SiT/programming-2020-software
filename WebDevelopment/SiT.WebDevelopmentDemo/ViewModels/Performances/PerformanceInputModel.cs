﻿using System.ComponentModel.DataAnnotations;

namespace SiT.WebDevelopmentDemo.ViewModels.Performances
{
    public class PerformanceInputModel
    {
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        public string Performer { get; set; }
        public double Duration { get; set; }
        public int EventId { get; set; }
    }
}
