﻿using SiT.FileSystem.Interfaces;
using System;
using System.Collections.Generic;

namespace SiT.FileSystem.Implementations
{
    class Folder : IFolder
    {
        private string name;
        private readonly List<IFolder> subFolders = new List<IFolder>();
        private readonly List<IFile> subFiles = new List<IFile>();

        public Folder(string name)
        {
            this.Name = name;
        }

        public string Icon { get; set; }

        public string Name 
        { 
            get => this.name;
            set
            {

                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException(nameof(value), "Any folder should have a valid name.");

                this.name = value;
            }
        }
        
        public IEnumerable<IFolder> SubFolders => this.subFolders;
        public int SubFoldersCount => this.subFolders.Count;

        public IEnumerable<IFile> SubFiles => this.subFiles;
        public int SubFilesCount => this.subFiles.Count;

        public IFileSystemParent Parent { get; set; }

        public string DisplayName => this.Name;
        public string FullPath 
        { 
            get
            {
                if (this.Parent != null)
                {
                    var pathToParent = this.Parent.FullPath;
                    return $"{pathToParent.TrimEnd('\\')}\\{this.DisplayName}";
                }
                else
                    return this.DisplayName;
            }
        }

        // CR: Review this code.
        public void AddSubFile(IFile file)
        {
            if (file == null)
                throw new ArgumentNullException(nameof(file));

            file.Parent = this;
            this.subFiles.Add(file);
        }

        public void AddSubFolder(IFolder folder)
        {
            if (folder == null)
                throw new ArgumentNullException(nameof(folder));

            folder.Parent = this;
            this.subFolders.Add(folder);
        }
    }
}
