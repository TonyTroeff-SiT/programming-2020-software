﻿namespace SiT.WebDevelopmentDemo.ViewModels.Performances
{
    public class PerformanceViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Perfromer { get; set; }
        public double Duration { get; set; }
    }
}
