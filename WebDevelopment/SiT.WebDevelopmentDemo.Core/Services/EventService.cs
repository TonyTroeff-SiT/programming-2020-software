﻿using SiT.WebDevelopmentDemo.Core.Interfaces;
using SiT.WebDevelopmentDemo.Data.Interfaces;
using SiT.WebDevelopmentDemo.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SiT.WebDevelopmentDemo.Core.Services
{
    public class EventService : IEventService
    {
        private readonly IEventRepository _eventRepository;

        public EventService(IEventRepository eventRepository)
        {
            this._eventRepository = eventRepository ?? throw new ArgumentNullException(nameof(eventRepository));
        }

        public async Task<Event> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            var requestedEvent = await this._eventRepository.GetAsync(e => e.Id == id, cancellationToken);
            return requestedEvent;
        }

        public async Task<IEnumerable<Event>> GetAllEventsAsync(CancellationToken cancellationToken = default)
        {
            var events = await this._eventRepository.GetManyAsync(e => true, cancellationToken);
            return events;
        }

        public async Task<IEnumerable<Event>> GetEventsForHostAsync(string hostName, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(hostName))
                return Enumerable.Empty<Event>();

            var events = await this._eventRepository.GetManyAsync(e => e.Host == hostName, cancellationToken);
            return events;
        }

        public async Task CreateEventAsync(Event eventToCreate, CancellationToken cancellationToken = default)
        {
            if (eventToCreate == null)
                return;

            await this._eventRepository.CreateAsync(eventToCreate, cancellationToken);
        }

        public async Task UpdateEventAsync(Event eventToUpdate, CancellationToken cancellationToken = default)
        {
            if (eventToUpdate == null)
                return;

            await this._eventRepository.UpdateAsync(eventToUpdate, cancellationToken);
        }

        public async Task DeleteEventAsync(int id, CancellationToken cancellationToken = default)
        {
            var existingEvent = await this._eventRepository.GetAsync(e => e.Id == id, cancellationToken);
            if (existingEvent == null)
                return;

            await this._eventRepository.DeleteAsync(existingEvent, cancellationToken);
        }
    }
}
