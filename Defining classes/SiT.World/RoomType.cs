﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiT.World
{
    public enum RoomType : byte
    {
        Kitchen,
        LivingRoom,
        DiningRoom,
        Bedroom
    }
}
