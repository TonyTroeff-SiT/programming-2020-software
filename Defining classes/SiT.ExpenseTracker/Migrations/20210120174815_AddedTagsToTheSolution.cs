﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiT.ExpenseTracker.Migrations
{
    public partial class AddedTagsToTheSolution : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TagTransfer",
                columns: table => new
                {
                    TagsId = table.Column<int>(type: "int", nullable: false),
                    TransfersId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagTransfer", x => new { x.TagsId, x.TransfersId });
                    table.ForeignKey(
                        name: "FK_TagTransfer_Tags_TagsId",
                        column: x => x.TagsId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TagTransfer_Transfers_TransfersId",
                        column: x => x.TransfersId,
                        principalTable: "Transfers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TagTransfer_TransfersId",
                table: "TagTransfer",
                column: "TransfersId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TagTransfer");

            migrationBuilder.DropTable(
                name: "Tags");
        }
    }
}
