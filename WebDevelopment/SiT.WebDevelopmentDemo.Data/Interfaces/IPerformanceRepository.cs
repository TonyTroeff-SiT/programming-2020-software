﻿using SiT.WebDevelopmentDemo.Data.Models;

namespace SiT.WebDevelopmentDemo.Data.Interfaces
{
    public interface IPerformanceRepository : IRepository<Performance>
    {
    }
}
