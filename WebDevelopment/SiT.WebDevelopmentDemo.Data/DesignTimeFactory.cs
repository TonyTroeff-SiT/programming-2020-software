﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace SiT.WebDevelopmentDemo.Data
{
    public class DesignTimeFactory : IDesignTimeDbContextFactory<DemoDbContext>
    {
        public DemoDbContext CreateDbContext(string[] args)
        {
            if (args is null || args.Length == 0)
                throw new InvalidOperationException("No sufficient arguments were provided.");

            var connectionString = args[0];
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new InvalidOperationException("No connection string was provided.");

            var optionsBuilder = new DbContextOptionsBuilder<DemoDbContext>();
            optionsBuilder.UseSqlServer(
                connectionString,
                sqlOptions =>
                {
                    sqlOptions.MigrationsHistoryTable("Migrations");
                });

            return new DemoDbContext(optionsBuilder.Options);
        }
    }
}
