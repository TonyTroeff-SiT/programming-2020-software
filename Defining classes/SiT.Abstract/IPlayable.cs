﻿namespace SiT.Abstract
{
    // Convention for naming any interface:
    // 1. Start with "I".
    // 2. In most cases they should end in "-able"
    public interface IPlayable
    {
        void PlayWithMe();
    }
}
