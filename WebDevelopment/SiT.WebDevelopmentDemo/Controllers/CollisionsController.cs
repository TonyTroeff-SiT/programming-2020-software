﻿using Microsoft.AspNetCore.Mvc;

namespace SiT.WebDevelopmentDemo.Controllers
{
    [Area("2104")]
    [Route("{area}-{controller}")]
    public class CollisionsController : Controller
    {
        // If the `Order` between two endpoints is different, we cannot reach a state of collision.
        // If the `Order` between two endpoints is **not** different, we can reach a state of collision in the following cases:
        // - When those endpoints are defined using the same attributes, r.g. [HttpGet] and [HttpGet] or [Route] and [Route].
        [Route("one", Order = 1)]
        public IActionResult First()
        {
            return this.Ok("1st");
        }

        [HttpGet("{action}", Order = 2)]
        public IActionResult One()
        {
            return this.Ok(1);
        }

        // [HttpGet("[action]/{email}")] -> Defined in such way the `email` is not allowed to contain slashes if they are not encoded in the url.
        // [HttpGet("[action]/{*email}")] -> Defined in such way the `email` parameter can contain slashes in any kind of representation.
        // [HttpGet("[action]/{**email}")] -> Simillar to the upper one with some slight differences over the slashes handling.
        [HttpGet("[action]/{**email}")]
        public IActionResult Register([FromRoute] string email)
        {
            return this.Ok(email);
        }

        [HttpGet("{action}/{x:int}")]
        [ActionName("Pow2")]
        public IActionResult Square(int x)
        {
            return this.Ok(x * x);
        }
    }
}
