﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiT.FileSystem.Interfaces
{
    interface IFolder : IFileSystemParent, IFileSystemEntity, IFileSystemChild
    {
        string Name { get; }

        //void Rename(string name);
    }
}
