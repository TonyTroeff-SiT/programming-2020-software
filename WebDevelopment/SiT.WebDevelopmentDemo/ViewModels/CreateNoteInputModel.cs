﻿namespace SiT.WebDevelopmentDemo.ViewModels
{
    public class CreateNoteInputModel
    {
        public string Text { get; set; }
        public string Theme { get; set; }
    }
}
