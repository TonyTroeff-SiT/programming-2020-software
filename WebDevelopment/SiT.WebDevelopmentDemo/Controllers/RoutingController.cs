﻿using Microsoft.AspNetCore.Mvc;

namespace SiT.WebDevelopmentDemo.Controllers
{
    [Route("theater")]
    public class RoutingController : Controller
    {
        // The attributes [HttpGet], [HttpPost] defined without an explicit routing template should represent the default action for this concrete http method for the controller.
        // If we define it on more than one method, we will recieve the following error:
        // Microsoft.AspNetCore.Routing.Matching.AmbiguousMatchException: The request matched multiple endpoints.
        [HttpGet]
        public IActionResult MyMethod()
        {
            return this.Ok(5);
        }

        // This is not valid (exception will be thrown): [HttpGet]
        [HttpPost]
        public IActionResult MySecondMethod()
        {
            return this.Ok(4);
        }

        // We can associate a single action of any controller with more than one endpoint.
        // This example shows us that we can even use the same method for handling various http methods.
        [HttpGet("arr")]
        [HttpGet("[action]")]
        [HttpDelete]
        [HttpPut("put")]
        public IActionResult Array()
        {
            return this.Ok(3);
        }

        // Defining the [Route] attribute for any controller action means that it will be executed no matter of the HTTP method.
        // This particular example will respond to Get, Post, Put, etc.
        [Route("myMatrix")]
        public IActionResult Matrix()
        {
            return this.Ok(2);
        }
        
        // We can still use some of the pre-defined endpoint matching templates with the [Route] attribute.
        [Route("[action]")]
        public IActionResult Database()
        {
            return this.Ok(1);
        }

        // We can have a collision in the endpoints between two methods defined with [Http*] and [Route] attributes.
        [HttpGet("[action]")]
        public IActionResult MyMatrix()
        {
            return this.Ok(0);
        }

        // We can still reach a state of collision even if in the first method we use text for the routing template and in the other "[action]".
        //[HttpGet("myMatrix")]
        //public IActionResult Test()
        //{
        //    return this.Ok(13);
        //}

        // We can still reach a state of collision between two endpoints defined with the [Route] attribute.
        // Unless we change the order.
        [Route("database", Order = 10)]
        public IActionResult Test()
        {
            return this.Ok(13);
        }
    }
}
