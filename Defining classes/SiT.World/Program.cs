﻿using System;

namespace SiT.World
{
    class Program
    {
        public static void Main()
        {
            var continent = new Continent();
            continent.Name = "Europe";
            continent.Area = 3.14;

            // {get;set;}
            //Console.WriteLine(continent.Countries2); // null
            //continent.Countries2 = new List<Country>();
            //Console.WriteLine(continent.Countries2.Count); // 0
            //continent.Countries2 = null;
            //Console.WriteLine(continent.Countries2.Count); // Error

            // Full property
            //Console.WriteLine(continent.Countries); // Instance of type List<Country>
            //continent.Countries = new List<Country>();
            //Console.WriteLine(continent.Countries.Count); // 0
            //continent.Countries = null; // Error
            //Console.WriteLine(continent.Countries.Count); 

            Console.WriteLine($"Continent: {continent.Name}; Area = {continent.Area}");

            var country = new Country();
            country.Name = "Bulgaria";
            country.Population = 6000000;
            country.Nature = CountryNature.Warm;
            country.Area = 111000;
            
            Console.WriteLine($"Country: {country.Name}; Area = {country.ConvertAreaToHectares()}; Population = {country.Population}; Nature = {country.Nature}");

            continent.Countries.Add(country);
            Console.WriteLine($"Count of countries: {continent.NumberOfCountries}");

            // NOTE: This will lead to a Compile-time error as the property is read-only.
            // continent.NumberOfCountries = 15;

            var city = new City();
            if (city.IsDiscrict) 
                Console.WriteLine("The city is district.");
            else 
                Console.WriteLine("It is an ordinary city.");
 
            country.Cities.Add(city);
            //country.Cities = null;

            var kitchen = new Room(100000);
            //kitchen.Area = 13.45;
            kitchen.Type = RoomType.Kitchen;
            // kitchen.Type = "Tony Troeff";

            Console.WriteLine($"Area in hectares: {kitchen.ConvertAreaToHectares()}");
            Console.WriteLine($"Price per night for the kitchen: {kitchen.CalculatePricePerNight()}");
        }
    }
}
