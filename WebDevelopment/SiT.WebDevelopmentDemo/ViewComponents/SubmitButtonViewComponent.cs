﻿using Microsoft.AspNetCore.Mvc;
using SiT.WebDevelopmentDemo.ViewModels;

namespace SiT.WebDevelopmentDemo.ViewComponents
{
    public class SubmitButtonViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string text)
        {
            var buttonData = new SubmitButtonData
            {
                ButtonText = text
            };
            return this.View(buttonData);
        }
    }
}
