﻿namespace SiT.World
{
    public class BaseEntity
    {
        protected BaseEntity(double area = 0)
        {
            this.Area = area;
        }

        public double Area { get; set; }
        public double HighestPoint { get; set; }

        public double ConvertAreaToHectares()
        {
            return this.Area / 10000;
        }
    }
}
