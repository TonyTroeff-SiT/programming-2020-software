﻿namespace SiT.FileSystem.Interfaces
{
    interface IFile : IFileSystemEntity, IFileSystemChild
    {
        string Name { get; }
        string Extension { get; }

        double Size { get; set; }

        void Rename(string newName, string newExtension);
    }
}
