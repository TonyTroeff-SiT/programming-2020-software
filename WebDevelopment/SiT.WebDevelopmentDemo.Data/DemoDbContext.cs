﻿using Microsoft.EntityFrameworkCore;
using SiT.WebDevelopmentDemo.Data.Models;

namespace SiT.WebDevelopmentDemo.Data
{
    public class DemoDbContext : DbContext
    {
        public DemoDbContext(DbContextOptions<DemoDbContext> options)
            : base(options)
        {
        }

        public DbSet<Event> Events { get; set; }

        public DbSet<Performance> Performances { get; set; }

        public DbSet<Note> Notes { get; set; }
    }
}
