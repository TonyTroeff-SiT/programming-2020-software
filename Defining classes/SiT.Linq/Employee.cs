﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SiT.LinqExamples
{
    class Employee
    {
        public Employee(string name, decimal salary)
        {
            this.Name = name;
            this.Salary = salary;
        }

        public Employee(string name, decimal salary, Employee boss)
            : this (name, salary)
        {
            this.Boss = boss;
        }

        public decimal Salary { get; }
        public string Name { get; }

        public Employee Boss { get; }
    }
}
