﻿using System;
using System.Collections.Generic;

namespace SiT.Social.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }
        public DateTime CreationTime { get; set; }

        public Blog Blog { get; set; }
        public ICollection<Tag> Tags { get; set; }
    }
}
