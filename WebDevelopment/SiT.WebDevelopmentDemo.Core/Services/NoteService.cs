﻿using SiT.WebDevelopmentDemo.Core.Interfaces;
using SiT.WebDevelopmentDemo.Data.Interfaces;
using SiT.WebDevelopmentDemo.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SiT.WebDevelopmentDemo.Core.Services
{
    public class NoteService : INoteService
    {
        private readonly INoteRepository _noteRepository;

        public NoteService(INoteRepository noteRepository)
        {
            this._noteRepository = noteRepository ?? throw new ArgumentNullException(nameof(noteRepository));
        }

        public async Task CreateAsync(Note note, CancellationToken cancellationToken = default)
        {
            await this._noteRepository.CreateAsync(note, cancellationToken);
        }

        public async Task<IEnumerable<Note>> GetAllNotesAsync(CancellationToken cancellationToken = default)
        {
            var notes = await this._noteRepository.GetManyAsync(x => true, cancellationToken);
            return notes;
        }
    }
}
