﻿using SiT.Gaming.Interfaces;

namespace SiT.Gaming.Implementations
{
    public class Poker : BaseCardGame, ICardGame
    {
        public Poker()
        {
            var cardDealer = new Dealer();
            var playerA = new CardGamesPlayer("Daniel");
            var playerB = new CardGamesPlayer("Joe");
            var playerC = new CardGamesPlayer("Mark");
            this.InternalPlayers.Add(cardDealer);
            this.InternalPlayers.Add(playerA);
            this.InternalPlayers.Add(playerB);
            this.InternalPlayers.Add(playerC);
        }

        public override string Name => "Poker";

        public override ushort RequiredNumberOfCards => 52;

        public override string GetRules()
        {
            return "Rules of poker";
        }
    }
}
