﻿namespace SiT.Gaming.Implementations
{
    public class CardGamesPlayer : BasePlayer
    {
        private readonly string _name;

        public CardGamesPlayer(string name)
        {
            this._name = name;
        }

        public override string DescribeMyself()
        {
            return $"My name is {this._name}";
        }
    }
}
