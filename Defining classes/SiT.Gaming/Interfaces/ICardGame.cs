﻿namespace SiT.Gaming.Interfaces
{
    public interface ICardGame : IGame
    {
        ushort RequiredNumberOfCards { get; }
    }
}
