﻿using SiT.Gaming.Interfaces;

namespace SiT.Gaming.Implementations
{
    public class AssassinsCreed : BaseGame, IDigitalGame
    {
        public AssassinsCreed()
        {
            var enemy = new Enemy();
            var odissey = new Odissey();
            this.InternalPlayers.Add(enemy);
            this.InternalPlayers.Add(odissey);
        }

        public override string Name => "Assassins Creed";

        public override string GetRules()
        {
            return "Rules of Assassins Creed";
        }
    }
}
