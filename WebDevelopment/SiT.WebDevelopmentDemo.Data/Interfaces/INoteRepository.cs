﻿using SiT.WebDevelopmentDemo.Data.Models;

namespace SiT.WebDevelopmentDemo.Data.Interfaces
{
    public interface INoteRepository : IRepository<Note>
    {
    }
}
