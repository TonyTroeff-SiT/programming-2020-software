﻿using SiT.FileSystem.Implementations;
using SiT.FileSystem.Interfaces;
using System;

namespace SiT.FileSystem
{
    class Program
    {
        static void Main()
        {
            var drive = new Drive("OS", 'C');
            PrintFileSystemEntityData(drive);

            var dataFolder = new Folder("Data");
            dataFolder.Parent = drive;
            PrintFileSystemEntityData(dataFolder);

            drive.AddSubFolder(dataFolder);
            // drive.AddSubFolder(dataFolder);

            var picturesFolder = new Folder("Pictures");
            drive.AddSubFolder(picturesFolder);
            PrintFileSystemEntityData(picturesFolder);

            Console.WriteLine(drive.SubFoldersCount);

            var gitignoreFile = new File(null, "gitignore");
            dataFolder.AddSubFile(gitignoreFile);

            // dataFolder.AddSubFile(gitignoreFile);

            gitignoreFile.Rename("New name", "gitignore");

            PrintFileSystemEntityData(gitignoreFile);

            // Drive
            // -> Data
            // -> Pictures
            // -> Data

            // TODO: Introduce Service pattern as rename may cause issues.
        }

        static void PrintFileSystemEntityData(IFileSystemEntity fileSystemEntity)
        {
            Console.WriteLine(fileSystemEntity.DisplayName);
            Console.WriteLine(fileSystemEntity.FullPath);
        }
    }
}
