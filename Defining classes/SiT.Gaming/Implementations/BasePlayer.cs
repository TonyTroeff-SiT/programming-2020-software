﻿using SiT.Gaming.Interfaces;

namespace SiT.Gaming.Implementations
{
    public abstract class BasePlayer : IPlayer
    {
        public virtual string DescribeMyself()
        {
            return "I am a basic player with no description.";
        }
    }
}
