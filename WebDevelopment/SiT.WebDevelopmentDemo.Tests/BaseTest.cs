﻿using Microsoft.Extensions.DependencyInjection;
using SiT.WebDevelopmentDemo.Core.Interfaces;
using SiT.WebDevelopmentDemo.Core.Services;
using SiT.WebDevelopmentDemo.Data.Interfaces;
using SiT.WebDevelopmentDemo.Data.Repositories;
using SiT.WebDevelopmentDemo.Tests.Infrastructure;
using System;
using Xunit.Abstractions;

namespace SiT.WebDevelopmentDemo.Tests
{
    public abstract class BaseTest : IDisposable
    {
        private readonly ITestDatabaseProvider _testDatabaseProvider;
        private readonly IServiceScope _testScope;
        private bool _isDisposed;

        protected ITestOutputHelper TestOutputHelper { get; }
        protected IServiceProvider ServiceProvider { get; }

        public BaseTest(ITestDatabaseProvider testDatabaseProvider, ITestOutputHelper testOutputHelper)
        {
            this.TestOutputHelper = testOutputHelper ?? throw new ArgumentNullException(nameof(testOutputHelper));
            this._testDatabaseProvider = testDatabaseProvider ?? throw new ArgumentNullException(nameof(testDatabaseProvider));
            this._testDatabaseProvider.Inject(testOutputHelper);

            var serviceCollection = new ServiceCollection();
            serviceCollection.AddScoped<IEventService, EventService>();
            serviceCollection.AddScoped<INoteService, NoteService>();
            serviceCollection.AddScoped<IPerformanceService, PerformanceService>();

            serviceCollection.AddScoped<IEventRepository, EventRepository>();
            serviceCollection.AddScoped<INoteRepository, NoteRepository>();
            serviceCollection.AddScoped<IPerformanceRepository, PerformanceRepository>();

            this._testDatabaseProvider.SetupDbContext(serviceCollection);
            var originalServiceProvider = serviceCollection.BuildServiceProvider();
            this._testScope = originalServiceProvider.CreateScope();
            this.ServiceProvider = this._testScope.ServiceProvider;
        }

        public void Dispose()
        {
            if (this._isDisposed)
                return;

            this._testDatabaseProvider.InvalidateOutputHelper();
            this._testScope.Dispose();
            this._isDisposed = true;
        }
    }
}
