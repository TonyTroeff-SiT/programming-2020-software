﻿using SiT.WebDevelopmentDemo.Core.Interfaces;
using SiT.WebDevelopmentDemo.Data.Interfaces;
using SiT.WebDevelopmentDemo.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SiT.WebDevelopmentDemo.Core.Services
{
    public class PerformanceService : IPerformanceService
    {
        private readonly IPerformanceRepository _performanceRepository;

        public PerformanceService(IPerformanceRepository performanceRepository)
        {
            this._performanceRepository = performanceRepository ?? throw new ArgumentNullException(nameof(performanceRepository));
        }

        public async Task CreateAsync(Performance performance, CancellationToken cancellationToken = default)
        {
            if (performance == null)
                return;

            await this._performanceRepository.CreateAsync(performance, cancellationToken);
        }

        public async Task<IReadOnlyCollection<Performance>> GetAllForEventAsync(int eventId, CancellationToken cancellationToken = default)
        {
            var performances = await this._performanceRepository.GetManyAsync(p => p.Event.Id == eventId);
            return performances.ToList().AsReadOnly();
        }
    }
}
