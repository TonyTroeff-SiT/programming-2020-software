﻿using System;
using System.Collections.Generic;

namespace SiT.World
{
    public class Continent : BaseNameIdentifiable
    {
        // Conventions:
        // Classes, Properties, Methods, Constructors - PascalCase
        // Variables, Field - camelCase

        // Property structure:
        // 1. Accessibility modifier - public
        // 2. Data type
        // 3. Name of the property
        // 4. { get; set; }

        // If a property is defined as follows:
        // public T Prop { get; }
        // it is assumed as read-only.

        // Field structure:
        // They should be private and should contain information only valuable for the instances of this class.
        // 1. Accessibility modifier - private
        // 2. Data type
        // 3. Name of the field
        // 4. ;

        // Constructor structure:
        // 1. Accessibility modifier - public
        // 2. Name of the class
        // 4. (...arguments)
        // 5. Body of the constructor

        private List<Country> countries;

        // Number of countries
        public int NumberOfCountries
        {
            get
            {
                return this.Countries.Count;
            }
        }

        // List of countries
        public List<Country> Countries
        {
            get
            {
                if (this.countries == null)
                    this.countries = new List<Country>();
                return this.countries;
            }

            set 
            {
                if (value == null)
                    throw new Exception("Cannot set null to Countries.");

                this.countries = value;
            }
        }

        //public List<Country> Countries2 { get; set; }

        public Continent()
        {
        }
    }
}
