﻿using SiT.WebDevelopmentDemo.Data.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SiT.WebDevelopmentDemo.Core.Interfaces
{
    public interface IPerformanceService
    {
        Task CreateAsync(Performance performance, CancellationToken cancellationToken = default);
        Task<IReadOnlyCollection<Performance>> GetAllForEventAsync(int eventId, CancellationToken cancellationToken = default);
    }
}
