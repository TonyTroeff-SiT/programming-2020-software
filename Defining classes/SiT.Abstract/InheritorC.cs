﻿namespace SiT.Abstract
{
    public class InheritorC : BaseClass
    {
        public override int Identifier { get; set; }

        public override string GetSomeText()
        {
            return "This is Inheritor C";
        }
    }
}
