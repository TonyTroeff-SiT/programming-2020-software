﻿namespace SiT.Gaming.Implementations
{
    public class Dealer : BasePlayer
    {
        public override string DescribeMyself()
        {
            return "I am just dealing cards";
        }
    }
}
