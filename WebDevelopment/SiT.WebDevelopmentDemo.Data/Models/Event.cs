﻿namespace SiT.WebDevelopmentDemo.Data.Models
{
    using System.Collections.Generic;

    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Host { get; set; }

        public ICollection<Performance> Performances { get; set; } = new List<Performance>();
    }
}
