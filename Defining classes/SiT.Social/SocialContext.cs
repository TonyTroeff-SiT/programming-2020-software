﻿namespace SiT.Social
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using SiT.Social.Models;
    using System;
    using System.Collections.Generic;

    public class SocialContext : DbContext
    {
        // 1. Property describing the Blogs table.
        public DbSet<Blog> Blogs { get; set; }

        // 2. Property describing the Posts table.
        public DbSet<Post> Posts { get; set; }

        // 3. Property describing the Tags table.
        public DbSet<Tag> Tags { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder
                .UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=SiT.Social;Trusted_Connection=True;")
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Blog>(blog =>
            {
                blog.HasMany(b => b.Posts)
                    .WithOne(p => p.Blog)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            // modelBuilder.Entity<Post>(
            //     post =>
            //     {
            //         post.HasOne(p => p.Blog)
            //             .WithMany(b => b.Posts)
            //             .OnDelete(DeleteBehavior.Cascade);
            //     });
            
            // modelBuilder.Entity<Post>(post =>
            // {
            //     post.HasMany(p => p.Tags)
            //         .WithMany(t => t.Posts)
            //         .UsingEntity<Dictionary<string, object>>(
            //             x => x.HasOne<Tag>().WithMany().OnDelete(DeleteBehavior.Cascade),
            //             x => x.HasOne<Post>().WithMany().OnDelete(DeleteBehavior.Cascade)
            //         );
            // });
        }
    }
}