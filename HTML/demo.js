// let <variable-name> = <value>;
// const <variable-name> = <value>;
// ! Deprecated: var <variable-name> = <value>;
let variableName = "Tony Troeff";
console.log("My name is " + variableName);

let unassignedVariable;
// default: undefined
console.log(typeof unassignedVariable);

let age = 18;
let isWorking = true;
let undefinedVariable = undefined;
let nullVariable = null;

// JSON - JavaScript Object Notation
let jsObject = {
  name: variableName,
  age: age,
  isWorking: isWorking,
  randomValue: null,
  town: "Shumen",
};
console.log(jsObject);

let jsArray = [0, 1, 2, 3, 5, 8, 92, 154];
jsArray[-1] = "Hello";
console.log(jsArray);

console.log(0.1 + 0.2);

// a == b (a != b)
console.log("2" == 2);
// a === b (a !== b)
console.log("2" === 2);

// a > b (a >= b)
// a < b (a <= b)

console.log(jsObject.town);
console.log(jsObject["town"]);

jsObject.country = "Bulgaria";
jsObject["planet"] = "Earth";
console.log(jsObject);

// Addition +
// Subraction -
// Multiplication *
// Division /
// Remainder %
// Exponential **

function isTruthy(value) {
  if (value) console.log("Truthy!");
  else console.log("Falsy!");
}

// Falsy!
isTruthy(undefined);
isTruthy(null);
isTruthy("");
isTruthy(0);
isTruthy(NaN);

// Truthy
isTruthy([]);
isTruthy({});
isTruthy(1);
isTruthy("Text");
isTruthy([0, 1, 2]);
isTruthy({ key: "value" });

console.log(true && true);
console.log(true || false);
console.log(!true);