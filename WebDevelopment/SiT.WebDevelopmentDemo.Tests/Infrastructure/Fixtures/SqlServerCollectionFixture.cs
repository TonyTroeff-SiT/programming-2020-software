﻿using Xunit;

namespace SiT.WebDevelopmentDemo.Tests.Infrastructure.Fixtures
{
    [CollectionDefinition(DEFINITION)]
    public class SqlServerCollectionFixture : ICollectionFixture<SqlServerDatabaseProvider>
    {
        public const string DEFINITION = "Sql server collection fixture definition";
    }
}
