﻿using System.Linq;
using System.Collections.Generic;
using System;

namespace SiT.LinqExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int> { 5, 17, 1, 36, 28, 159 };
            var michael = new Employee("Michael", 5);
            var john1 = new Employee("John", 1);
            var john2 = new Employee("John", 36);

            List<Employee> employees = new List<Employee>
            {
                michael,
                new Employee("Teodor", 17, john1),
                john1,
                john2,
                new Employee("Ben", 28, michael),
                new Employee("Ben", 6, michael),
                new Employee("Ben", 13, michael),
                new Employee("Egor", 159, john2),
                new Employee("George", -15)
            };

            // O (n^2) 8 000 -> 64 000 000
            // O (n) 8 000 -> 8 000

            //var minSalary = decimal.MaxValue;
            //Employee employeeWithMinSalary = null;

            //foreach (var employee in employees)
            //{
            //    if (employee.Salary >= minSalary)
            //        continue;

            //    minSalary = employee.Salary;
            //    employeeWithMinSalary = employee;
            //}

            var minSalary = employees.Min(e => e.Salary);
            System.Console.WriteLine(minSalary);

            var maxSalary = employees.Max(e => e.Salary);
            System.Console.WriteLine(maxSalary);

            // Take all employees that have a boss.
            var uniqueNames = employees
                .Where(e => e.Boss != null)
                .Select(e => e.Boss)
                .Where(b => b.Salary < 10)
                .Select(b => b.Name)
                .Distinct();
            System.Console.WriteLine(string.Join(", ", uniqueNames));

            // Take the first employee by name
            var firstJohn = employees.FirstOrDefault(e => e.Name == "John");
            System.Console.WriteLine("First John salary: " + firstJohn.Salary);

            var lastJohn = employees.LastOrDefault(e => e.Name == "John");
            System.Console.WriteLine("Last John salary: " + lastJohn.Salary);

            //foreach (var e in employees.OrderBy(e => e.Salary))
            //    System.Console.WriteLine($"{e.Name} -> {e.Salary}");

            // Retrieve all employees whose salary is above 10, ordered by their name and then by their salary in descending order.
            // 1. First filter the collection so less entities are then sorted.
            foreach (var e in employees
                .Where(e => e.Salary > 10)
                .OrderBy(e => e.Name)
                .ThenByDescending(e => e.Salary))
            {
                System.Console.WriteLine($"{e.Name} -> {e.Salary}");
            }

            // Get the only employee by name.
            var singleMichael = employees.SingleOrDefault(e => e.Name == "Michael");
            if (singleMichael == null) System.Console.WriteLine("No Michaels are found");
            else System.Console.WriteLine($"Only Michael's salary {singleMichael.Salary}");

            // All ->
            // If all entries of a collection match a given criteria, true is returned.
            System.Console.WriteLine(employees.All(e => e.Salary > 0));
            System.Console.WriteLine(employees.All(e => e.Name == "Ben"));

            // Any ->
            // If any entry of a collection matches a given criteria, true is returned.
            System.Console.WriteLine(employees.Any(e => e.Salary > 0));
            System.Console.WriteLine(employees.Any(e => e.Name == "Ben"));

            System.Console.WriteLine(employees.Any(e => e.Salary < 0));

            decimal totalSum = 0m;
            foreach (var employee in employees)
            {
                totalSum += employee.Salary;
            }

            decimal average = totalSum / employees.Count;

            decimal totalSum2 = 0m;
            foreach (var employee in employees)
            {
                if (employee.Name != "Ben" && employee.Salary > 0 && employee.Salary > average)
                {
                    totalSum2 += employee.Salary;
                }
            }

            System.Console.WriteLine(totalSum2);

            var averageSalary = employees.Average(e => e.Salary);
            System.Console.WriteLine(employees
                .Where(e => e.Name != "Ben" && e.Salary > 0 && e.Salary > averageSalary)
                .Sum(e => e.Salary));
        }
    }
}
