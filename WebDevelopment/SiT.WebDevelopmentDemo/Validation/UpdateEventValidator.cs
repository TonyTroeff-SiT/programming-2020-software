﻿using FluentValidation;
using SiT.WebDevelopmentDemo.ViewModels;

namespace SiT.WebDevelopmentDemo.Validation
{
    //public class UpdateEventValidator : BaseEventValidator<UpdateEventInputModel>
    //{
    //    public UpdateEventValidator() 
    //        : base()
    //    {
    //        this.RuleFor(x => x.Id).GreaterThan(0);
    //    }
    //}

    public class UpdateEventValidator : AbstractValidator<UpdateEventInputModel>
    {
        public UpdateEventValidator(IValidator<BaseEventInputModel> baseValidator)
        {
            this.Include(baseValidator);
            this.RuleFor(x => x.Id).GreaterThan(0);
        }
    }
}
