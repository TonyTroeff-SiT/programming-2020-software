﻿using SiT.Gaming.Interfaces;

namespace SiT.Gaming.Implementations
{
    public class Enemy : BasePlayer
    {
        public override string DescribeMyself()
        {
            return "I am evil enemy";
        }
    }
}
