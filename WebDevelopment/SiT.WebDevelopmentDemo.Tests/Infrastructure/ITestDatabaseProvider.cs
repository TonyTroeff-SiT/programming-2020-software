﻿using Microsoft.Extensions.DependencyInjection;
using Xunit.Abstractions;

namespace SiT.WebDevelopmentDemo.Tests.Infrastructure
{
    public interface ITestDatabaseProvider
    {
        void Inject(ITestOutputHelper testOutputHelper);
        void InvalidateOutputHelper();
        void SetupDbContext(IServiceCollection serviceCollection);
    }
}
