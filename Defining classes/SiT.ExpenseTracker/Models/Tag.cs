﻿using System.Collections.Generic;

namespace SiT.ExpenseTracker.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<Transfer> Transfers { get; set; }
    }
}
