﻿using System;
using System.Collections.Generic;

namespace SiT.Commercial
{
    public class Company
    {
        private List<Product> ownedProducts = new List<Product>();
        private List<Product> futureProducts = new List<Product>();
        private List<Product> frozenProducts = new List<Product>();
        private List<Employee> employees = new List<Employee>();

        // Name
        public string Name { get; set; }

        // Year of establishment
        public uint YearOfEstablishment { get; set; }

        // Activity (enum)
        public Activity Activity { get; set; }

        // Owned Products
        public List<Product> OwnedProducts
        {
            get => this.ownedProducts;
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Cannot set null to `Owned Products`");

                this.ownedProducts = value;
            }
        }

        // Future Products
        public List<Product> FutureProducts
        {
            get => this.futureProducts;
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Cannot set null to `Future Products`");

                this.futureProducts = value;
            }
        }

        // Frozen Products
        public List<Product> FrozenProducts
        {
            get => this.frozenProducts;
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Cannot set null to `Frozen Products`");

                this.frozenProducts = value;
            }
        }

        // Total products count = Owned + Future + Frozen
        public int TotalProductsCount => this.ownedProducts.Count + this.futureProducts.Count + this.frozenProducts.Count;

        // Employees
        public List<Employee> Employees
        {
            get => this.employees;
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Cannot set null to `Employees`");

                this.employees = value;
            }
        }

        // Count of Employees
        public int CountOfEmployees => this.employees.Count;
    }
}
