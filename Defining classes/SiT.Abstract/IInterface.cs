﻿namespace SiT.Abstract
{
    // Interface is much like a description of what one instance is capable of doing.
    // By default all described members are public.
    // There is no available implementation.
    public interface IInterface
    {
        // Defining properties.
        // The only difference is that we omit the accessibility modifier.
        // Only automatic properties are available for definition.
        int MyFavoriteNumber { get; set; }

        // Defining methods.
        // The only difference is that we omit the accessibility modifier.
        // The methods do not have implementation.
        string MyFavoriteNovel(string author);
    }
}
