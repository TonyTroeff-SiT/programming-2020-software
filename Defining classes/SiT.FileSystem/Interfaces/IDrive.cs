﻿namespace SiT.FileSystem.Interfaces
{
    interface IDrive : IFileSystemParent, IFileSystemEntity
    {
        string Name { get; }

        char Identifier { get; set; }

        //void Rename(string name);
    }
}
