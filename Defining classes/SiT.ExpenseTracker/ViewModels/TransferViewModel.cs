﻿namespace SiT.ExpenseTracker.ViewModels
{
    public class TransferViewModel
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public decimal Amount { get; set; }
    }
}
