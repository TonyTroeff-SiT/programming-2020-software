﻿using Microsoft.EntityFrameworkCore;
using SiT.WebDevelopmentDemo.Data.Interfaces;
using SiT.WebDevelopmentDemo.Data.Models;

namespace SiT.WebDevelopmentDemo.Data.Repositories
{
    public class EventRepository : BaseRepository<Event>, IEventRepository
    {
        public EventRepository(DemoDbContext dbContext) : base(dbContext)
        {
        }

        protected override DbSet<Event> DbSet => this.DbContext.Events;
    }
}
