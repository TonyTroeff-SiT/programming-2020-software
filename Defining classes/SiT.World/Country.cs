﻿using System;
using System.Collections.Generic;

namespace SiT.World
{
    public class Country : BaseNameIdentifiable
    {
        private List<City> cities;

        // Population
        public ulong Population { get; set; }

        // Nature
        public CountryNature Nature { get; set; }

        // List of all cities within the current country.
        public List<City> Cities
        {
            get 
            {
                // Check if the private field is initialized.
                if (this.cities == null)
                    this.cities = new List<City>();

                return this.cities;
            }

            set
            {
                // Check if the value is null. Prevent this.
                if (value == null)
                    throw new Exception("You cannot set null to the Cities property.");

                this.cities = value;
            }
        }
    }
}
