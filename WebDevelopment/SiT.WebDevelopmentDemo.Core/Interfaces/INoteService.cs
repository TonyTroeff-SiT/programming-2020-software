﻿using SiT.WebDevelopmentDemo.Data.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SiT.WebDevelopmentDemo.Core.Interfaces
{
    public interface INoteService
    {
        Task CreateAsync(Note note, CancellationToken cancellationToken = default);
        Task<IEnumerable<Note>> GetAllNotesAsync(CancellationToken cancellationToken = default);
    }
}
