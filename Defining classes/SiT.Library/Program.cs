﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace SiT.Library
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Notes:
            // We prepared a relation between the Author and Book entity.
            // This relation is called One-To-Many (from the perspective of the Author).
            // This relation is called Many-To-One (from the perspective of the Book).

            // Commands to run:
            // dotnet tool install --global dotnet-ef
            // dotnet ef migrations add InitialCreate
            // dotnet ef database update

            // 1 - Create a school
            // Enter school name: 
            // Enter school type:
            // etc.
            // 2 - Create a class
            // 3 - Create a student

            // 4 - Get all schools
            // 5 - Get all classes
            // 6 - Get all students
            // 7 - Get all classes for school
            // Enter the school identifier: 

            // 8 - Get all students for school and class
            // Enter the school identifier: 
            // Enter the class identifier: 

            var libraryDbContext = new LibraryDbContext();

            //var author = new Author
            //{
            //    Age = 24,
            //    Name = "John Davis",
            //    Nationality = "Canadian"
            //};
            //var book = new Book
            //{
            //    Name = "Walk in the forest",
            //    Rating = 7.2,
            //    Author = author
            //};

            //libraryDbContext.Authors.Add(author);
            //libraryDbContext.Books.Add(book);
            //await libraryDbContext.SaveChangesAsync();

            var allAuthors = await libraryDbContext.Authors.Where(a => a.Age <= 20).OrderBy(a => a.Name).ToListAsync();
            foreach (var author in allAuthors)
            {
                System.Console.WriteLine(author.Id);
                System.Console.WriteLine(author.Name);
                System.Console.WriteLine(author.Age);
                System.Console.WriteLine(author.Nationality);
            }
        }
    }
}
