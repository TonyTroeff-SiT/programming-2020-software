﻿namespace SiT.Abstract
{
    public abstract class BaseClass
    {
        public abstract int Identifier { get; set; }

        // This is an abstract method.
        public abstract string GetSomeText();

        // This is a virtual method.
        // Virtual methods provide us with a default implementation.
        // Some of the inheritors may use it as it is; some may override it.
        // Some of the inheritors may use it partially and then enrich this functionality.
        public virtual decimal GetSomeMoney()
        {
            return 3.14m;
        }

        // This is an ordinary method.
        // Those methods cannot be overriden. Their behavior cannot be modified from the inheritors.
        public double Calculate()
        {
            return 5.3 * 9.75;
        }
    }
}
