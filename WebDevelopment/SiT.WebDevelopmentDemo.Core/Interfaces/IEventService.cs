﻿namespace SiT.WebDevelopmentDemo.Core.Interfaces
{
    using SiT.WebDevelopmentDemo.Data.Models;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IEventService
    {
        Task<Event> GetByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<IEnumerable<Event>> GetAllEventsAsync(CancellationToken cancellationToken = default);
        Task<IEnumerable<Event>> GetEventsForHostAsync(string hostName, CancellationToken cancellationToken = default);
        Task CreateEventAsync(Event eventToCreate, CancellationToken cancellationToken = default);
        Task UpdateEventAsync(Event eventToUpdate, CancellationToken cancellationToken = default);
        Task DeleteEventAsync(int id, CancellationToken cancellationToken = default);
    }
}
