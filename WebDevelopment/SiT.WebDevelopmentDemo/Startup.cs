using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SiT.WebDevelopmentDemo.Core.Interfaces;
using SiT.WebDevelopmentDemo.Core.Services;
using SiT.WebDevelopmentDemo.Data;
using SiT.WebDevelopmentDemo.Data.Interfaces;
using SiT.WebDevelopmentDemo.Data.Repositories;
using SiT.WebDevelopmentDemo.Models;
using SiT.WebDevelopmentDemo.Validation;
using SiT.WebDevelopmentDemo.ViewModels;
using System;

namespace SiT.WebDevelopmentDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // AddSingleton
            // AddScoped
            // AddTransient
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<INoteService, NoteService>();
            services.AddScoped<IPerformanceService, PerformanceService>();

            services.AddScoped<IEventRepository, EventRepository>();
            services.AddScoped<INoteRepository, NoteRepository>();
            services.AddScoped<IPerformanceRepository, PerformanceRepository>();

            var databaseSettings = this.Configuration.GetSection("DatabaseSettings").Get<DatabaseSettings>();
            
            // The following priciple is equivalent to the existing one.
            // var databaseSettings = new DatabaseSettings();
            // this.Configuration.GetSection("DatabaseSettings").Bind(databaseSettings);
            
            // This requires the values to be written at the root level:
            // var databaseSettings = this.Configuration.Get<DatabaseSettings>();

            services.AddDbContext<DemoDbContext>(options =>
            {
                options.UseSqlServer(databaseSettings.ConnectionString);
            });
            services.AddControllersWithViews();

            services.AddMvc().AddFluentValidation();
            services.AddTransient<IValidator<BaseEventInputModel>, BaseEventValidator>();
            services.AddTransient<IValidator<CreateEventInputModel>, CreateEventValidator>();
            services.AddTransient<IValidator<UpdateEventInputModel>, UpdateEventValidator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                // endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}");
            });
        }
    }
}
