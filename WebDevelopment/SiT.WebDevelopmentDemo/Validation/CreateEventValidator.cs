﻿using FluentValidation;
using SiT.WebDevelopmentDemo.ViewModels;

namespace SiT.WebDevelopmentDemo.Validation
{
    //public class CreateEventValidator : BaseEventValidator<CreateEventInputModel>
    //{
    //}

    public class CreateEventValidator : AbstractValidator<CreateEventInputModel>
    {
        public CreateEventValidator(IValidator<BaseEventInputModel> baseValidator)
        {
            this.Include(baseValidator);
        }
    }
}
