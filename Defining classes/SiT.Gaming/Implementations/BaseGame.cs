﻿using SiT.Gaming.Interfaces;
using System.Collections.Generic;

namespace SiT.Gaming.Implementations
{
    public abstract class BaseGame : IGame
    {
        protected readonly List<IPlayer> InternalPlayers = new List<IPlayer>();

        public abstract string Name { get; }

        public virtual string GetRules()
        {
            return "No information";
        }

        public IEnumerable<IPlayer> Players => this.InternalPlayers;
        public int PlayersCount => this.InternalPlayers.Count;
    }
}
