﻿using SiT.FileSystem.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SiT.FileSystem.Implementations
{
    class Drive : IDrive
    {
        private string name;
        private char identifier;
        private List<IFolder> folders = new List<IFolder>();
        private List<IFile> files = new List<IFile>();

        public Drive(string name, char identifier)
        {
            this.Name = name;
            this.Identifier = identifier;
        }

        public string Icon { get; set; }
        public string Name
        {
            get => this.name; 
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException(nameof(value), "Any drive should have a valid name.");

                this.name = value;
            }
        }

        public char Identifier 
        { 
            get => this.identifier;
            set
            {
                if (char.IsLetter(value) == false)
                    throw new ArgumentNullException(nameof(value), "Any drive should have a valid identifier.");

                this.identifier = value;
            }
        }

        public IEnumerable<IFolder> SubFolders => this.folders;
        public int SubFoldersCount => this.folders.Count;

        public IEnumerable<IFile> SubFiles => this.files;
        public int SubFilesCount => this.files.Count;

        public string DisplayName => $"{this.Name} ({this.Identifier}:)";
        public string FullPath => $"{this.Identifier}:\\";

        public void AddSubFile(IFile file)
        {
            if (file == null)
                throw new ArgumentNullException(nameof(file));

            // If any file has the same name and extension, throw an exception.
            if (this.files.Any(f => f.Name == file.Name && f.Extension == file.Extension))
                throw new Exception("A file with the same name and extension already exists.");

            file.Parent = this;
            this.files.Add(file);
        }

        public void AddSubFolder(IFolder folder)
        {
            if (folder == null)
                throw new ArgumentNullException(nameof(folder));

            // If any folder has the same name, throw an exception.
            if (this.folders.Any(f => f.Name == folder.Name))
                throw new Exception("A folder with the same name already exists.");

            folder.Parent = this;
            this.folders.Add(folder);
        }
    }
}
