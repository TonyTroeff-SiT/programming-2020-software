﻿using System.Collections.Generic;

namespace SiT.FileSystem.Interfaces
{
    interface IFileSystemParent : IFileSystemEntity
    {
        IEnumerable<IFolder> SubFolders { get; }
        int SubFoldersCount { get; }

        IEnumerable<IFile> SubFiles { get; }
        int SubFilesCount { get; }

        void AddSubFolder(IFolder folder);
        void AddSubFile(IFile file);
    }
}
