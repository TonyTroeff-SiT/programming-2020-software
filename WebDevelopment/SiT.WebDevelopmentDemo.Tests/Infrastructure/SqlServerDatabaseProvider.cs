﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiT.WebDevelopmentDemo.Data;
using System;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace SiT.WebDevelopmentDemo.Tests.Infrastructure
{
    public class SqlServerDatabaseProvider : ITestDatabaseProvider, IAsyncLifetime
    {
        private ITestOutputHelper _testOutputHelper; 
        
        /// <inheritdoc />
        public DemoDbContext DbContext { get; }

        public SqlServerDatabaseProvider()
        {
            var optionsBuilder = new DbContextOptionsBuilder<DemoDbContext>();
            this.SetupDbContextOptions(optionsBuilder);

            this.DbContext = new DemoDbContext(optionsBuilder.Options);
        }

        public Task InitializeAsync() => this.DbContext.Database.MigrateAsync();

        public void Inject(ITestOutputHelper testOutputHelper)
        {
            this._testOutputHelper = testOutputHelper;
        }

        public void InvalidateOutputHelper()
        {
            this._testOutputHelper = null;
        }

        public void SetupDbContext(IServiceCollection serviceCollection)
        {
            if (serviceCollection is null)
                throw new InvalidOperationException("No service collection was provided to the sql server test database provider.");

            serviceCollection.AddDbContext<DemoDbContext>(options => this.SetupDbContextOptions(options));
        }
        
        private void SetupDbContextOptions(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost\TONYTROEFF;Database=SiT.WebDevelopmentDemo_Tests;Trusted_Connection=True;");
            optionsBuilder.LogTo(this.Log, LogLevel.Information);
            optionsBuilder.EnableSensitiveDataLogging();
        }

        public Task DisposeAsync()
        {
            return this.DbContext.Database.EnsureDeletedAsync();
        }

        private void Log(string message)
        {
            if (this._testOutputHelper != null)
                this._testOutputHelper.WriteLine(message);
        }
    }
}
