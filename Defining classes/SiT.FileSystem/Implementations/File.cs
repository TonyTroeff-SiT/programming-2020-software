﻿using SiT.FileSystem.Interfaces;
using System;

namespace SiT.FileSystem.Implementations
{
    class File : IFile
    {
        private string name;
        private string extension;

        public File(string name, string extension)
        {
            if (string.IsNullOrWhiteSpace(name) && string.IsNullOrWhiteSpace(extension))
                throw new Exception("The file should have a valid full name.");

            this.name = name;
            this.extension = extension;
        }

        public string Icon { get; set; }

        public string Name => this.name;
        public string Extension => this.extension;

        public double Size { get; set; }
        public IFileSystemParent Parent { get; set; }

        public string DisplayName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Name))
                    return $".{this.Extension}";
                else if (string.IsNullOrWhiteSpace(this.Extension))
                    return this.Name;
                else
                    return $"{this.Name}.{this.Extension}";
            }
        }

        public string FullPath
        {
            get
            {
                if (this.Parent != null)
                {
                    var pathToParent = this.Parent.FullPath;
                    return $"{pathToParent.TrimEnd('\\')}\\{this.DisplayName}";
                }
                else
                    return this.DisplayName;
            }
        }

        public void Rename(string newName, string newExtension)
        {
            if (string.IsNullOrWhiteSpace(newName) && string.IsNullOrWhiteSpace(newExtension))
                throw new Exception("The file should have a valid full name.");

            // We should throw an exception if the name and the extension is not changed.
            if (newName == this.name && newExtension == this.extension)
                throw new Exception("Name is not changed.");

            this.name = newName;
            this.extension = newExtension;
        }
    }
}
