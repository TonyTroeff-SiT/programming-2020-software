﻿namespace SiT.Gaming.Interfaces
{
    public interface IPlayer
    {
        string DescribeMyself();
    }
}
