﻿using Microsoft.EntityFrameworkCore;

namespace SiT.Library
{
    public class LibraryDbContext : DbContext
    {
        // To give enough information to EF core for our tables, we just define some properties of type `DbSet<>`
        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=SiT.LibraryDemo;Trusted_Connection=True;");
        }
    }
}
