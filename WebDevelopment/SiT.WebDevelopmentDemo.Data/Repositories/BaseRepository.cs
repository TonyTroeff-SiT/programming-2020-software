﻿using Microsoft.EntityFrameworkCore;
using SiT.WebDevelopmentDemo.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace SiT.WebDevelopmentDemo.Data.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected DemoDbContext DbContext { get; }
        protected abstract DbSet<TEntity> DbSet { get; }

        protected BaseRepository(DemoDbContext dbContext)
        {
            this.DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task CreateAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            if (entity == null)
                return;

            var dbSet = this.GetDbSet();
            dbSet.Add(entity);
            await this.DbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            if (entity == null)
                return;

            var dbSet = this.GetDbSet();
            dbSet.Update(entity);
            await this.DbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            if (entity == null)
                return;

            var dbSet = this.GetDbSet();
            dbSet.Remove(entity);
            await this.DbContext.SaveChangesAsync();
        }

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> condition, CancellationToken cancellationToken = default)
        {
            if (condition == null)
                return null;

            var result = await this.GetDbSet().FirstOrDefaultAsync(condition, cancellationToken);
            return result;
        }

        public async Task<IEnumerable<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> condition, CancellationToken cancellationToken = default)
        {
            if (condition == null)
                return Enumerable.Empty<TEntity>();

            var result = await this.GetDbSet().Where(condition).ToListAsync(cancellationToken);
            return result;
        }

        private DbSet<TEntity> GetDbSet()
        {
            if (this.DbSet == null)
                throw new InvalidOperationException("The underlying DbSet is null");

            return this.DbSet;
        }
    }
}
