﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

namespace SiT.LinqExercises
{
    class Program
    {
        static void Main(string[] args)
        {
            Exercise12();
        }

        private static void Exercise1()
        {
            var n = int.Parse(Console.ReadLine());
            var numbers = new List<int>();

            for (int i = 0; i < n; i++)
                numbers.Add(int.Parse(Console.ReadLine()));

            Console.WriteLine($"Min: {numbers.Min()}");
            Console.WriteLine($"Max: {numbers.Max()}");
            Console.WriteLine($"Sum: {numbers.Sum()}");
            Console.WriteLine($"Average: {numbers.Average()}");
        }

        private static void Exercise2()
        {
            // 1 5 9 32 65

            // 1. Console.ReadLine() => "1 5 9 32 65"
            // 2. Split(' ', StringSplitOptions.RemoveEmptyEntries) => ["1", "5", "9", "32", "65"]
            // 3. Select(...) => [1m, 5m, 9m, 32m, 65m]
            // 4. ToList()
            var numbers = Console.ReadLine()
                .Split(' ')
                .Select(x => decimal.Parse(x))
                .ToList();

            var orderedNumbers = numbers.OrderBy(d => d);
            Console.WriteLine(string.Join(" <= ", orderedNumbers));
        }

        private static void Exercise3()
        {
            var numbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => int.Parse(x))
                .ToList();

            // 1. Find the squre root.
            // 2. If the squre root is whole number, the given numer is a square number.
            var result = numbers.Where(x => Math.Sqrt(x) % 1 == 0).OrderByDescending(x => x);
            Console.WriteLine(string.Join(" ", result));
        }

        private static void Exercise4()
        {
            var numbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => int.Parse(x))
                .ToList();

            var filteredNumbers = numbers.Where(x => x >= 0).Reverse();

            // `.Any()` will return `true` if the collection contains any elements. 
            // And will return `false` if the collection is empty.
            if (filteredNumbers.Any()) Console.WriteLine(string.Join(" ", filteredNumbers));
            else Console.WriteLine("empty");
        }

        private static void Exercise5()
        {
            var numbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => int.Parse(x))
                .ToList();

            var arguments = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => int.Parse(x))
                .ToList();
            Debug.Assert(arguments.Count == 3);

            var take = arguments[0];

            // [12, 25, 6, 84, 72]
            // Take 20, Skip n -> This is invalid case
            // Take 2, Skip 3 -> This is invalid case
            // Take 2, Skip 1 -> This is valid
            Debug.Assert(take <= numbers.Count);

            var skip = arguments[1];
            Debug.Assert(take >= skip);

            var numberToSearch = arguments[2];

            var chosenNumbers = numbers.Take(take).Skip(skip);
            if (chosenNumbers.Contains(numberToSearch))
                Console.WriteLine("YES!");
            else
                Console.WriteLine("NO!");
        }

        private static void Exercise6()
        {
            var numbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => int.Parse(x))
                .ToList();

            var topThreeNumbers = numbers.OrderByDescending(x => x).Take(3);
            Console.WriteLine(string.Join(" ", topThreeNumbers));
        }

        private static void Exercise7()
        {
            var text = Console.ReadLine().ToLower();

            var separators = new char[] { '.', ',', ':', ';', '(', ')', '[', ']', '"', '\'', '\\', '/', '!', '?', ' ' };
            var words = text.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            var shortWords = words.Where(x => x.Length < 5);

            // Home, sweet home
            // words = ["Home", "sweet", "home"]
            // shortWords = ["Home", "home"]
            var result = shortWords/*.Select(x => x.ToLower())*/.Distinct().OrderBy(x => x);
            Console.WriteLine(string.Join(", ", result));
        }

        private static void Exercise8()
        {
            var numbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => int.Parse(x))
                .ToList();

            var evenNumbers = numbers.Where(x => x % 2 == 0);
            var average = evenNumbers.Average();

            var result = evenNumbers.Select(x =>
            {
                if (x > average)
                    return x + 1;
                else
                    return x - 1;
            });

            Console.WriteLine(string.Join(" ", result));
        }

        private static void Exercise9()
        {
            //var words = new string[] { "1", "10", "11", "100" };
            //Console.WriteLine(string.Join(" ", words.OrderBy(x => x)));

            var times = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => TimeSpan.ParseExact(x, "hh\\:mm", CultureInfo.InvariantCulture));

            Console.WriteLine(string.Join(", ", times.OrderBy(x => x).Select(x => x.ToString("hh\\:mm"))));
        }

        private static void Exercise10()
        {
            var numbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => int.Parse(x))
                .ToList();
            Debug.Assert(numbers.Count % 4 == 0);

            int k = numbers.Count / 4;

            // These operations have complexity of O(2 * n)
            //int[] firstRow = numbers.Take(k).Reverse().Concat(numbers.Reverse().Take(k).ToArray()).ToArray();
            //int[] secondRow = numbers.Skip(k).Take(2 * k).ToArray();

            // These three loops have complexity of O(n)
            var firstRow = new List<int>(capacity: k * 2);
            for (int i = k - 1; i >= 0; i--)
                firstRow.Add(numbers[i]);
            for (int i = 0; i < k; i++)
                firstRow.Add(numbers[numbers.Count - (i + 1)]);

            var secondRow = new List<int>(capacity: k * 2);
            for (int i = 0; i < k * 2; i++)
                secondRow.Add(numbers[k + i]);

            //First solution:
            //var result = firstRow.Select((x, index) => x + secondRow[index]);

            //Second solution:
            //var result = new int[k * 2];
            //for (int i = 0; i < k * 2; i++)
            //    result[i] = firstRow[i] + secondRow[i];

            // Third solution:
            var result = new List<int>(capacity: k * 2);
            foreach (var (firstRowItem, secondRowItem) in firstRow.Zip(secondRow))
                result.Add(firstRowItem + secondRowItem);

            Console.WriteLine(string.Join(" ", result));
        }

        private static void Exercise11()
        {
            var data = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Where(x => x.Length == 2);

            var numbers = new List<byte>();
            foreach (var element in data)
            {
                var reversedElement = new string(element.Reverse().ToArray());

                // We cannot use int/byte.Parse because the input may be hexadecimal.
                numbers.Add(Convert.ToByte(reversedElement, 16));
            }

            // 1. Reverse the numbers collection.
            // 2. Iterate the numbers collection backwards.
            for (int i = numbers.Count - 1; i >= 0; i--)
                Console.Write((char)numbers[i]);
        }

        private static void Exercise12()
        {
            var input = Console.ReadLine();

            var digits = new List<int>();
            var symbols = new List<char>();

            foreach (var character in input)
            {
                if (char.IsDigit(character))
                    digits.Add(int.Parse(character.ToString()));
                else 
                    symbols.Add(character);
            }

            StringBuilder sb = new StringBuilder();

            // 1. Skip & Take
            //for (int i = 0; i < digits.Count; i++)
            //{
            //    var currentNumber = digits[i];
            //    if (currentNumber == 0)
            //        continue;

            //    if (i % 2 == 0)
            //    {
            //        var text = symbols.Take(currentNumber).ToArray();
            //        sb = sb.Append(text);
            //    }

            //    symbols = symbols.Skip(currentNumber).ToList();
            //}

            // 2. Without Skip & Take
            int indexer = 0;
            for (int i = 0; i < digits.Count; i++)
            {
                var currentNumber = digits[i];
                if (currentNumber == 0)
                    continue;

                if (i % 2 == 0)
                {
                    for (int j = 0; j < currentNumber && indexer + j < symbols.Count; j++)
                        sb = sb.Append(symbols[indexer + j]);
                }

                indexer += currentNumber;
            }


            Console.WriteLine(sb.ToString());
        }
    }
}
