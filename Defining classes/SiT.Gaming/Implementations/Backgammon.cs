﻿using SiT.Gaming.Interfaces;

namespace SiT.Gaming.Implementations
{
    public class Backgammon : BaseGame, IBoardGame
    {
        public override string Name => "Backgammon";
    }
}
