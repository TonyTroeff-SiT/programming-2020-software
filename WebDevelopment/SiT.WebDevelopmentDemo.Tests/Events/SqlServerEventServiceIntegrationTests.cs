﻿using SiT.WebDevelopmentDemo.Tests.Infrastructure;
using SiT.WebDevelopmentDemo.Tests.Infrastructure.Fixtures;
using Xunit;
using Xunit.Abstractions;

namespace SiT.WebDevelopmentDemo.Tests.Events
{
    [Collection(SqlServerCollectionFixture.DEFINITION)]
    public class SqlServerEventServiceIntegrationTests : EventServiceIntegrationTest
    {
        public SqlServerEventServiceIntegrationTests(SqlServerDatabaseProvider testDatabaseProvider, ITestOutputHelper testOutputHelper) : base(testDatabaseProvider, testOutputHelper)
        {
        }
    }
}
