﻿namespace SiT.ExpenseTracker.ViewModels
{
    using System.Collections.Generic;

    public class TagViewModel
    {
        private readonly List<TransferViewModel> _transferViewModels = new List<TransferViewModel>();

        public int Id { get; set; }
        public string Name { get; set; }
        public IReadOnlyCollection<TransferViewModel> Transfers => _transferViewModels.AsReadOnly();

        public void AddTransferViewModel(TransferViewModel transferViewModel)
        {
            if (transferViewModel is null)
                return;

            this._transferViewModels.Add(transferViewModel);
        }
    }
}
