﻿namespace SiT.Commercial
{
    public enum Activity
    {
        Technology,
        Accounting,
        Food,
        Clothing,
        Transport
    }
}