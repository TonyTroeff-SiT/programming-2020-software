﻿namespace SiT.WebDevelopmentDemo.Data.Models
{
    public class Performance
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PerformerName { get; set; }
        public double Duration { get; set; }

        // int EventId { get; set; }
        public Event Event { get; set; }
    }
}
