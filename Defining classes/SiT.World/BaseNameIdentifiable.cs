﻿namespace SiT.World
{
    public class BaseNameIdentifiable : BaseEntity
    {
        public string Name { get; set; }
    }
}
