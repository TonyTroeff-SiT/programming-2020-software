﻿using SiT.Gaming.Implementations;
using SiT.Gaming.Interfaces;
using System;

namespace SiT.Gaming
{
    class Program
    {
        static void Main()
        {
            var poker = new Poker();
            DescribeCardGame(poker);

            var assassinsCreed = new AssassinsCreed();
            DescribeGame(assassinsCreed);
        }

        private static void DescribeGame(IGame game)
        {
            Console.WriteLine(game.Name);
            Console.WriteLine(game.GetRules());
            Console.WriteLine(game.PlayersCount);

            foreach (var player in game.Players)
                Console.WriteLine(player.DescribeMyself());
        }

        private static void DescribeCardGame(ICardGame game)
        {
            DescribeGame(game);
            Console.WriteLine(game.RequiredNumberOfCards);
        }
    }
}
