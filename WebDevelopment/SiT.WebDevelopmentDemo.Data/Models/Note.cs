﻿namespace SiT.WebDevelopmentDemo.Data.Models
{
    public class Note
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Theme { get; set; }
    }
}
