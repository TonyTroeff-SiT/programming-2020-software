﻿namespace SiT.FileSystem.Interfaces
{
    interface IFileSystemEntity
    {
        // Drive -> OS (C:)
        // Folder -> "Name of the folder"
        // File -> "Name of the file"."Extension of the file"
        string DisplayName { get; }

        string Icon { get; set; }

        // Drives -> C:\
        // Folders & Files -> Computed property - concat the names of each parent recursively and the name of the current folder.
        string FullPath { get; }
    }
}
