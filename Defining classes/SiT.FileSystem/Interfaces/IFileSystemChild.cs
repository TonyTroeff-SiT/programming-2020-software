﻿namespace SiT.FileSystem.Interfaces
{
    interface IFileSystemChild : IFileSystemEntity
    {
        IFileSystemParent Parent { get; set; }
    }
}
