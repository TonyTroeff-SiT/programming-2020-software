﻿using System;

namespace SiT.Abstract
{
    class Program
    {
        static void Main()
        {
            // Cannot create an instance of an abstract type.
            // var baseClass = new BaseClass();
            var inheritorA = new InheritorA();
            Console.WriteLine(inheritorA.GetSomeMoney());
            PlayWithSomeone(inheritorA);
            ShowMyFavoriteBook(inheritorA);

            var inheritorB = new InheritorB();
            Console.WriteLine(inheritorB.GetSomeMoney());

            var inheritorC = new InheritorC();
            Console.WriteLine(inheritorC.GetSomeMoney());
        }

        static void PlayWithSomeone(IPlayable playable)
        {
            playable.PlayWithMe();
        }

        static void ShowMyFavoriteBook(IInterface instance)
        {
            Console.WriteLine(instance.MyFavoriteNovel("Troeff"));
        }
    }
}
