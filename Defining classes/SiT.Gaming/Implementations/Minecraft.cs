﻿using SiT.Gaming.Interfaces;

namespace SiT.Gaming.Implementations
{
    public class Minecraft : BaseGame, IDigitalGame
    {
        public override string Name => "Minecraft";
    }
}
